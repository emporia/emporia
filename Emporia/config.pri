# Auto-generated by IDE. Changes by user will be lost!
BASEDIR =  $$quote($$_PRO_FILE_PWD_)

device {
    CONFIG(debug, debug|release) {
        SOURCES +=  $$quote($$BASEDIR/src/BL/BLNewsDetailsItem.cpp) \
                 $$quote($$BASEDIR/src/BL/BLNewsItem.cpp) \
                 $$quote($$BASEDIR/src/BL/BLNewsItemLoader.cpp) \
                 $$quote($$BASEDIR/src/BL/BLOpeningHoursItem.cpp) \
                 $$quote($$BASEDIR/src/BL/BLPage0Hem.cpp) \
                 $$quote($$BASEDIR/src/BL/BLPage1Butiker.cpp) \
                 $$quote($$BASEDIR/src/BL/BLPage2Aktuellt.cpp) \
                 $$quote($$BASEDIR/src/BL/BLPage3Oppettider.cpp) \
                 $$quote($$BASEDIR/src/BL/BLPageBase.cpp) \
                 $$quote($$BASEDIR/src/BL/BLShopDetailsItem.cpp) \
                 $$quote($$BASEDIR/src/BL/BLShopItem.cpp) \
                 $$quote($$BASEDIR/src/BL/BLSlideItem.cpp) \
                 $$quote($$BASEDIR/src/Network/NetworkPage0Hem.cpp) \
                 $$quote($$BASEDIR/src/Network/NetworkPage1Butiker.cpp) \
                 $$quote($$BASEDIR/src/Network/NetworkPage2Aktuellt.cpp) \
                 $$quote($$BASEDIR/src/Network/NetworkPage3Oppettider.cpp) \
                 $$quote($$BASEDIR/src/Utilities/QtObjectFormatter.cpp) \
                 $$quote($$BASEDIR/src/Utilities/StatusEventHandler.cpp) \
                 $$quote($$BASEDIR/src/Utilities/StringUtils.cpp) \
                 $$quote($$BASEDIR/src/Utilities/WebImageView.cpp) \
                 $$quote($$BASEDIR/src/applicationui.cpp) \
                 $$quote($$BASEDIR/src/main.cpp)

        HEADERS +=  $$quote($$BASEDIR/src/BL/BLNewsDetailsItem.h) \
                 $$quote($$BASEDIR/src/BL/BLNewsItem.h) \
                 $$quote($$BASEDIR/src/BL/BLNewsItemLoader.h) \
                 $$quote($$BASEDIR/src/BL/BLOpeningHoursItem.h) \
                 $$quote($$BASEDIR/src/BL/BLPage0Hem.h) \
                 $$quote($$BASEDIR/src/BL/BLPage1Butiker.h) \
                 $$quote($$BASEDIR/src/BL/BLPage2Aktuellt.h) \
                 $$quote($$BASEDIR/src/BL/BLPage3Oppettider.h) \
                 $$quote($$BASEDIR/src/BL/BLPageBase.h) \
                 $$quote($$BASEDIR/src/BL/BLShopDetailsItem.h) \
                 $$quote($$BASEDIR/src/BL/BLShopItem.h) \
                 $$quote($$BASEDIR/src/BL/BLSlideItem.h) \
                 $$quote($$BASEDIR/src/Network/NetworkPage0Hem.h) \
                 $$quote($$BASEDIR/src/Network/NetworkPage1Butiker.h) \
                 $$quote($$BASEDIR/src/Network/NetworkPage2Aktuellt.h) \
                 $$quote($$BASEDIR/src/Network/NetworkPage3Oppettider.h) \
                 $$quote($$BASEDIR/src/Utilities/QtObjectFormatter.hpp) \
                 $$quote($$BASEDIR/src/Utilities/StatusEventHandler.h) \
                 $$quote($$BASEDIR/src/Utilities/StringUtils.h) \
                 $$quote($$BASEDIR/src/Utilities/WebImageView.h) \
                 $$quote($$BASEDIR/src/applicationui.hpp)

    }

    CONFIG(release, debug|release) {
        SOURCES +=  $$quote($$BASEDIR/src/BL/BLNewsDetailsItem.cpp) \
                 $$quote($$BASEDIR/src/BL/BLNewsItem.cpp) \
                 $$quote($$BASEDIR/src/BL/BLNewsItemLoader.cpp) \
                 $$quote($$BASEDIR/src/BL/BLOpeningHoursItem.cpp) \
                 $$quote($$BASEDIR/src/BL/BLPage0Hem.cpp) \
                 $$quote($$BASEDIR/src/BL/BLPage1Butiker.cpp) \
                 $$quote($$BASEDIR/src/BL/BLPage2Aktuellt.cpp) \
                 $$quote($$BASEDIR/src/BL/BLPage3Oppettider.cpp) \
                 $$quote($$BASEDIR/src/BL/BLPageBase.cpp) \
                 $$quote($$BASEDIR/src/BL/BLShopDetailsItem.cpp) \
                 $$quote($$BASEDIR/src/BL/BLShopItem.cpp) \
                 $$quote($$BASEDIR/src/BL/BLSlideItem.cpp) \
                 $$quote($$BASEDIR/src/Network/NetworkPage0Hem.cpp) \
                 $$quote($$BASEDIR/src/Network/NetworkPage1Butiker.cpp) \
                 $$quote($$BASEDIR/src/Network/NetworkPage2Aktuellt.cpp) \
                 $$quote($$BASEDIR/src/Network/NetworkPage3Oppettider.cpp) \
                 $$quote($$BASEDIR/src/Utilities/QtObjectFormatter.cpp) \
                 $$quote($$BASEDIR/src/Utilities/StatusEventHandler.cpp) \
                 $$quote($$BASEDIR/src/Utilities/StringUtils.cpp) \
                 $$quote($$BASEDIR/src/Utilities/WebImageView.cpp) \
                 $$quote($$BASEDIR/src/applicationui.cpp) \
                 $$quote($$BASEDIR/src/main.cpp)

        HEADERS +=  $$quote($$BASEDIR/src/BL/BLNewsDetailsItem.h) \
                 $$quote($$BASEDIR/src/BL/BLNewsItem.h) \
                 $$quote($$BASEDIR/src/BL/BLNewsItemLoader.h) \
                 $$quote($$BASEDIR/src/BL/BLOpeningHoursItem.h) \
                 $$quote($$BASEDIR/src/BL/BLPage0Hem.h) \
                 $$quote($$BASEDIR/src/BL/BLPage1Butiker.h) \
                 $$quote($$BASEDIR/src/BL/BLPage2Aktuellt.h) \
                 $$quote($$BASEDIR/src/BL/BLPage3Oppettider.h) \
                 $$quote($$BASEDIR/src/BL/BLPageBase.h) \
                 $$quote($$BASEDIR/src/BL/BLShopDetailsItem.h) \
                 $$quote($$BASEDIR/src/BL/BLShopItem.h) \
                 $$quote($$BASEDIR/src/BL/BLSlideItem.h) \
                 $$quote($$BASEDIR/src/Network/NetworkPage0Hem.h) \
                 $$quote($$BASEDIR/src/Network/NetworkPage1Butiker.h) \
                 $$quote($$BASEDIR/src/Network/NetworkPage2Aktuellt.h) \
                 $$quote($$BASEDIR/src/Network/NetworkPage3Oppettider.h) \
                 $$quote($$BASEDIR/src/Utilities/QtObjectFormatter.hpp) \
                 $$quote($$BASEDIR/src/Utilities/StatusEventHandler.h) \
                 $$quote($$BASEDIR/src/Utilities/StringUtils.h) \
                 $$quote($$BASEDIR/src/Utilities/WebImageView.h) \
                 $$quote($$BASEDIR/src/applicationui.hpp)

    }

}

simulator {
    CONFIG(debug, debug|release) {
        SOURCES +=  $$quote($$BASEDIR/src/BL/BLNewsDetailsItem.cpp) \
                 $$quote($$BASEDIR/src/BL/BLNewsItem.cpp) \
                 $$quote($$BASEDIR/src/BL/BLNewsItemLoader.cpp) \
                 $$quote($$BASEDIR/src/BL/BLOpeningHoursItem.cpp) \
                 $$quote($$BASEDIR/src/BL/BLPage0Hem.cpp) \
                 $$quote($$BASEDIR/src/BL/BLPage1Butiker.cpp) \
                 $$quote($$BASEDIR/src/BL/BLPage2Aktuellt.cpp) \
                 $$quote($$BASEDIR/src/BL/BLPage3Oppettider.cpp) \
                 $$quote($$BASEDIR/src/BL/BLPageBase.cpp) \
                 $$quote($$BASEDIR/src/BL/BLShopDetailsItem.cpp) \
                 $$quote($$BASEDIR/src/BL/BLShopItem.cpp) \
                 $$quote($$BASEDIR/src/BL/BLSlideItem.cpp) \
                 $$quote($$BASEDIR/src/Network/NetworkPage0Hem.cpp) \
                 $$quote($$BASEDIR/src/Network/NetworkPage1Butiker.cpp) \
                 $$quote($$BASEDIR/src/Network/NetworkPage2Aktuellt.cpp) \
                 $$quote($$BASEDIR/src/Network/NetworkPage3Oppettider.cpp) \
                 $$quote($$BASEDIR/src/Utilities/QtObjectFormatter.cpp) \
                 $$quote($$BASEDIR/src/Utilities/StatusEventHandler.cpp) \
                 $$quote($$BASEDIR/src/Utilities/StringUtils.cpp) \
                 $$quote($$BASEDIR/src/Utilities/WebImageView.cpp) \
                 $$quote($$BASEDIR/src/applicationui.cpp) \
                 $$quote($$BASEDIR/src/main.cpp)

        HEADERS +=  $$quote($$BASEDIR/src/BL/BLNewsDetailsItem.h) \
                 $$quote($$BASEDIR/src/BL/BLNewsItem.h) \
                 $$quote($$BASEDIR/src/BL/BLNewsItemLoader.h) \
                 $$quote($$BASEDIR/src/BL/BLOpeningHoursItem.h) \
                 $$quote($$BASEDIR/src/BL/BLPage0Hem.h) \
                 $$quote($$BASEDIR/src/BL/BLPage1Butiker.h) \
                 $$quote($$BASEDIR/src/BL/BLPage2Aktuellt.h) \
                 $$quote($$BASEDIR/src/BL/BLPage3Oppettider.h) \
                 $$quote($$BASEDIR/src/BL/BLPageBase.h) \
                 $$quote($$BASEDIR/src/BL/BLShopDetailsItem.h) \
                 $$quote($$BASEDIR/src/BL/BLShopItem.h) \
                 $$quote($$BASEDIR/src/BL/BLSlideItem.h) \
                 $$quote($$BASEDIR/src/Network/NetworkPage0Hem.h) \
                 $$quote($$BASEDIR/src/Network/NetworkPage1Butiker.h) \
                 $$quote($$BASEDIR/src/Network/NetworkPage2Aktuellt.h) \
                 $$quote($$BASEDIR/src/Network/NetworkPage3Oppettider.h) \
                 $$quote($$BASEDIR/src/Utilities/QtObjectFormatter.hpp) \
                 $$quote($$BASEDIR/src/Utilities/StatusEventHandler.h) \
                 $$quote($$BASEDIR/src/Utilities/StringUtils.h) \
                 $$quote($$BASEDIR/src/Utilities/WebImageView.h) \
                 $$quote($$BASEDIR/src/applicationui.hpp)

    }

}

INCLUDEPATH +=  $$quote($$BASEDIR/src/Network) \
         $$quote($$BASEDIR/src/BL) \
         $$quote($$BASEDIR/src) \
         $$quote($$BASEDIR/src/Utilities)

CONFIG += precompile_header

PRECOMPILED_HEADER =  $$quote($$BASEDIR/precompiled.h)

lupdate_inclusion {
    SOURCES +=  $$quote($$BASEDIR/../assets/*.qml)

}

TRANSLATIONS =  $$quote($${TARGET}_sv.ts) \
         $$quote($${TARGET}.ts)

