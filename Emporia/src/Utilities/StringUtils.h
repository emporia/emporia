/*
 * StringUtils.h
 *
 *  Created on: 18 feb 2013
 *      Author: eiteds
 */

#ifndef STRINGUTILS_H_
#define STRINGUTILS_H_

#include <QString>
#include <string>

using namespace std;

class StringUtils {
public:
	static int QStringToInt(QString msg);
	static string QStringToString(QString msg);
	static QString intToQString(int value);
};

#endif /* STRINGUTILS_H_ */
