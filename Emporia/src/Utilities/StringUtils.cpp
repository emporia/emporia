/*
 * StringUtils.cpp
 *
 *  Created on: 18 feb 2013
 *      Author: eiteds
 */

#include "StringUtils.h"
#include <sstream>

using namespace std;

int StringUtils::QStringToInt(QString msg) {
	stringstream ss(msg.toAscii().data());
	int value;
	ss >> value;
	return value;
}

string StringUtils::QStringToString(QString msg) {
	return string (msg.toUtf8().data());
}

QString StringUtils::intToQString(int value) {
	stringstream ss;
	ss << value;
	return QString(ss.str().c_str());
}
