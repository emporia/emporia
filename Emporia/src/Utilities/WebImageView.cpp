/*
 * WebImageView.cpp
 *
 *  Created on: 4 oct. 2012
 *      Author: schutz
 *
 *  Modified: eiteds, 21 March 2013
 */

#include "WebImageView.h"

#include <bb/cascades/DockLayout>
#include <bb/cascades/Image>

#include <QtGui/QImage>

using namespace bb;
using namespace bb::cascades;

QNetworkAccessManager * WebImageView::mNetManager = new QNetworkAccessManager;

WebImageView::WebImageView() {
	mCropImage = false;
	mScaleToFitImage = false;
}

WebImageView::WebImageView(bool crop, bool scaletofit) {
	WebImageView();
	mCropImage = crop;
	mScaleToFitImage = scaletofit;
}

const QUrl& WebImageView::url() const {
	return mUrl;
}

bool WebImageView::scaletofit() const {
	return mScaleToFitImage;
}

void WebImageView::setUrl(const QUrl& url) {
	mUrl = url;
	mLoading = 0;
	QNetworkReply * reply = mNetManager->get(QNetworkRequest(url));
	connect(reply,SIGNAL(finished()), this, SLOT(imageLoaded()));
	connect(reply,SIGNAL(downloadProgress ( qint64 , qint64  )), this, SLOT(dowloadProgressed(qint64,qint64)));
	setLayout(new DockLayout());
	mActivityIndicator.setPreferredSize(50, 50);
	mActivityIndicator.setHorizontalAlignment(HorizontalAlignment::Center);
	mActivityIndicator.setVerticalAlignment(VerticalAlignment::Center);
	mActivityIndicator.start();
	mActivityIndicator.setOverlapTouchPolicy(OverlapTouchPolicy::Allow);
	add(&mActivityIndicator);
	emit urlChanged();
}

void WebImageView::setScaleToFit(bool scaletofit) {
	mScaleToFitImage = scaletofit;
	emit scaletofitChanged();
}

double WebImageView::loading() const {
	return mLoading;
}

void WebImageView::imageLoaded() {
	QNetworkReply * reply = qobject_cast<QNetworkReply*>(sender());
	QByteArray array = reply->readAll();
	if (!mCropImage) {
		mImageView.setImage(Image(array));
	} else {
		QImage image_q;
		image_q.loadFromData(array);
		QImage image_copy = image_q.copy(image_q.width()-900, 0, 768, 420);
        const bb::ImageData imageData = bb::ImageData::fromPixels(image_copy.bits(), bb::PixelFormat::RGBX, image_copy.width(), image_copy.height(), image_copy.bytesPerLine());
		mImageView.setImage(Image(imageData));
	}
	if (mScaleToFitImage) {
		mImageView.setScalingMethod(ScalingMethod::AspectFit);
	}
	mImageView.setPreferredWidth(preferredWidth());
	mImageView.setPreferredHeight(preferredHeight());
	mImageView.setOverlapTouchPolicy(OverlapTouchPolicy::Allow);
	add(&mImageView);
	setOverlapTouchPolicy(OverlapTouchPolicy::Allow);
	mActivityIndicator.stop();
	qDebug() << "Downloaded: " << reply->url().toString(QUrl::None) << "data" << array.size();
	reply->deleteLater();
}

void WebImageView::dowloadProgressed(qint64 bytes,qint64 total) {
	mLoading =  double(bytes)/double(total);
	emit loadingChanged();
}
