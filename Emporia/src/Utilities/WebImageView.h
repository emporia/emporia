/*
 * WebImageView.h
 *
 *  Created on: 4 oct. 2012
 *      Author: schutz
 */

#ifndef WEBIMAGEVIEW_H_
#define WEBIMAGEVIEW_H_

#include <bb/cascades/Container>
#include <bb/cascades/ActivityIndicator>
#include <bb/cascades/ImageView>

#include <QNetworkAccessManager>
#include <QUrl>

using namespace bb::cascades;

class WebImageView : public Container {
	Q_OBJECT
	Q_PROPERTY (QUrl url READ url WRITE setUrl NOTIFY urlChanged)
	Q_PROPERTY (bool scaletofit READ scaletofit WRITE setScaleToFit NOTIFY scaletofitChanged)
	Q_PROPERTY (float loading READ loading NOTIFY loadingChanged)

public:
	WebImageView();
	WebImageView(bool crop, bool scaletofit);
	const QUrl& url() const;
	bool scaletofit() const;
	double loading() const;

	public Q_SLOTS:
	void setUrl(const QUrl& url);
	void setScaleToFit(const bool scaletofit);

	private Q_SLOTS:
	void imageLoaded();
	void dowloadProgressed(qint64,qint64);

signals:
	void urlChanged();
	void loadingChanged();
	void scaletofitChanged();

private:
	bool mCropImage;
	bool mScaleToFitImage;
	QUrl mUrl;
	float mLoading;
	ActivityIndicator mActivityIndicator;
	ImageView mImageView;

	static QNetworkAccessManager * mNetManager;
};

#endif /* WEBIMAGEVIEW_H_ */
