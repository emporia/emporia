/*
 * NetworkPage3Oppettider.cpp
 *
 *  Created on: 7 mar 2013
 *      Author: eiteds
 */

#include "NetworkPage3Oppettider.h"

#include "../BL/BLPage3Oppettider.h"
#include "../applicationui.hpp"

#include <bb/data/DataAccessError>
#include <bb/data/JsonDataAccess>

#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QDebug>
#include <QUrl>

using namespace bb::data;

NetworkPage3Oppettider::NetworkPage3Oppettider(BLPage3Oppettider &context) : mContext(context) {
	qDebug() << "NetworkPage3Oppettider::NetworkPage3Oppettider()";
}

NetworkPage3Oppettider::~NetworkPage3Oppettider() {
	qDebug() << "NetworkPage3Oppettider::~NetworkPage3Oppettider()";
}

std::vector<BLOpeningHoursItem> const& NetworkPage3Oppettider::getOpeningHoursVector() {
	return mOpeningHoursVector;
}

void NetworkPage3Oppettider::loadOpeningHoursFromServer() {
	qDebug() << "NetworkPage3Oppettider::loadOpeningHoursFromServer()";
	QUrl url(ApplicationUI::EMPORIA_API_URL() + "/api/openinghours/get/");
	qDebug() << url.toString(QUrl::None);
    QNetworkRequest request(url);

    QNetworkAccessManager *networkAccessManager = new QNetworkAccessManager(this);
    networkAccessManager->get(request);
    connect(networkAccessManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(onGetReplyOpeningHoursList(QNetworkReply*)));
}

void NetworkPage3Oppettider::onGetReplyOpeningHoursList(QNetworkReply *reply) {
	qDebug() << "NetworkPage3Oppettider::onGetReplyOpeningHoursList()";
    QString response;
    if (reply) {
        if (reply->error() == QNetworkReply::NoError) {
            const int available = reply->bytesAvailable();

            if (available > 0) {
                const QByteArray buffer(reply->readAll());

                JsonDataAccess ja;
                const QVariant jsonva = ja.loadFromBuffer(QString::fromUtf8(buffer));

                if (ja.hasError()) {
                    // Retrieve the error
                    DataAccessError theError = ja.error();

                    // Determine the type of error that occurred
                    if (theError.errorType() != DataAccessErrorType::None) {
                    	mContext.activityDone("JSon Parsing Error: " + ja.error().errorMessage());
                    	reply->deleteLater();
                        return;
                    }
                } else {
                	QList<QVariant> itemlist = jsonva.toList();

            		QList<QVariant>::iterator itemit = itemlist.begin();
                	for (; itemit != itemlist.end(); ++itemit) {
                		QMap<QString, QVariant> itemMap = itemit->toMap();
                		// go through ordinary list section
                		QList<QVariant> ordinaryList = itemMap["ordinary"].toList();
                    	QList<QVariant>::iterator ordinaryit = ordinaryList.begin();
                    	for (; ordinaryit != ordinaryList.end(); ++ordinaryit) {
                    		QMap<QString, QVariant> ordinaryMap = ordinaryit->toMap();
                    		BLOpeningHoursItem item;
                    		item.mCategory = itemMap["category"].toString();
                    		item.mName = ordinaryMap["name"].toString();
                    		item.mIsOrdinary = true;
                    		item.mIsPublished = (!ordinaryMap["published"].toString().compare("true")) ? true: false;
                    		item.mIsSpecial = false;
                    		item.mOpened = ordinaryMap["opened"].toString();
                    		item.mIsSpecialVisible = false;
                    		mOpeningHoursVector.push_back(item);
                    	}
                    	// go through special list section
                		QList<QVariant> specialList = itemMap["special"].toList();
                		QMap<QString, QVariant> specialMap = itemMap["special"].toMap();
                    	QList<QVariant>::iterator specialit = specialList.begin();
                    	for (; specialit != specialList.end(); ++specialit) {
                    		QMap<QString, QVariant> sMap = specialit->toMap();
                    		BLOpeningHoursItem item;
                    		item.mCategory = itemMap["category"].toString();
                    		item.mName = sMap["name"].toString();
                    		item.mIsOrdinary = true;
                    		item.mIsPublished = (!sMap["published"].toString().compare("true")) ? true: false;
                    		item.mIsSpecial = false;
                    		item.mOpened = sMap["opened"].toString();
                    		item.mIsSpecialVisible = (!specialMap["visible"].toString().compare("true")) ? true: false;
                    		mOpeningHoursVector.push_back(item);
                    	}
                	}
                }
            }
        } else {
        	mContext.activityDone("Network problem: " + reply->errorString());
        	reply->deleteLater();
            return;
        }
    }

	mContext.activityDone("Success");
    reply->deleteLater();
}
