/*
 * NetworkPage3Oppettider.h
 *
 *  Created on: 7 mar 2013
 *      Author: eiteds
 */

#ifndef NETWORKPAGE3OPPETTIDER_H_
#define NETWORKPAGE3OPPETTIDER_H_

#include <../BL/BLNewsItem.h>
#include <../BL/BLOpeningHoursItem.h>

#include <QtCore/QObject>

#include <vector>

class BLPage3Oppettider;
class QNetworkReply;

class NetworkPage3Oppettider : public QObject {
	Q_OBJECT
public:
	NetworkPage3Oppettider(BLPage3Oppettider &context);
	virtual ~NetworkPage3Oppettider();

	void loadOpeningHoursFromServer();
	std::vector<BLOpeningHoursItem> const& getOpeningHoursVector();

private Q_SLOTS:
    void onGetReplyOpeningHoursList(QNetworkReply *reply);

private:
    std::vector<BLOpeningHoursItem> mOpeningHoursVector;

    BLPage3Oppettider &mContext;
};

#endif /* NETWORKPAGE3OPPETTIDER_H_ */
