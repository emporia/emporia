/*
 * NetworkPage2Aktuellt.h
 *
 *  Created on: 5 mar 2013
 *      Author: eiteds
 */

#ifndef NETWORKPAGE2AKTUELLT_H_
#define NETWORKPAGE2AKTUELLT_H_

#include <../BL/BLNewsItem.h>

#include <QtCore/QObject>

#include <vector>

class QNetworkReply;
class BLPage2Aktuellt;

class NetworkPage2Aktuellt : public QObject {
	Q_OBJECT
public:
	NetworkPage2Aktuellt(BLPage2Aktuellt &context);
	virtual ~NetworkPage2Aktuellt();

	void loadNewsFromServer(int page);
	void loadNewsFromServerWithId(const QString &id);

	std::vector<BLNewsItem> const& getNewsItemVector();

private Q_SLOTS:
    void onGetReplyNewsList(QNetworkReply* reply);
    void onGetReplyNewsId(QNetworkReply*);

private:
    std::vector<BLNewsItem> mNewsVector;

    BLPage2Aktuellt &mContext;
};

#endif /* NETWORKPAGE2AKTUELLT_H_ */
