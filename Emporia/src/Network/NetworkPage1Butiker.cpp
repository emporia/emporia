/*
 * NetworkPage1Butiker.cpp
 *
 *  Created on: 28 feb 2013
 *      Author: eiteds
 */


#include "NetworkPage1Butiker.h"

#include "../BL/BLPage1Butiker.h"
#include "../BL/BLShopItem.h"
#include "../BL/BLShopDetailsItem.h"
#include "../Utilities/StringUtils.h"
#include "../applicationui.hpp"

#include <bb/data/DataAccessError>
#include <bb/data/JsonDataAccess>

#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QDebug>
#include <QUrl>

using namespace bb::data;

NetworkPage1Butiker::NetworkPage1Butiker(BLPage1Butiker &context) : mContext(context) {
}

NetworkPage1Butiker::~NetworkPage1Butiker() {
}

std::vector<BLShopItem> const& NetworkPage1Butiker::getShopItemVector() {
	return mShopVector;
}

void NetworkPage1Butiker::loadShopsFromServer() {
	qDebug() << "PageBL1Butiker::loadShopsFromServer()";
	QUrl url("http://api.emporia.se/api/shop/get");
    QNetworkRequest request(url);

    QNetworkAccessManager *networkAccessManager = new QNetworkAccessManager(this);
    networkAccessManager->get(request);
    connect(networkAccessManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(onGetReplyShopList(QNetworkReply*)));
}

void NetworkPage1Butiker::onGetReplyShopList(QNetworkReply* reply) {
	qDebug() << "PageBL1Butiker::onGetReplyShopList()";
    QString response;
    if (reply) {
        if (reply->error() == QNetworkReply::NoError) {
            const int available = reply->bytesAvailable();

            if (available > 0) {
                const QByteArray buffer(reply->readAll());
                JsonDataAccess ja;
                const QVariant jsonva = ja.loadFromBuffer(QString::fromUtf8(buffer));
                if (ja.hasError()) {
                    // Retrieve the error
                    DataAccessError theError = ja.error();

                    // Determine the type of error that occurred
                    if (theError.errorType() != DataAccessErrorType::None) {
                    	mContext.activityDone("JSon Parsing Error: " + ja.error().errorMessage());
                    	reply->deleteLater();
                    	return;
                    }
                } else {
                	QList<QVariant> itemlist = jsonva.toList();
                	QList<QVariant>::iterator itemit = itemlist.begin();
                	for (; itemit != itemlist.end(); ++itemit) {
                		QMap<QString, QVariant> map = itemit->toMap();
                		BLShopItem item;

                		item.mId = map["id"].toString();
                		if (!map["pageName"].toString().trimmed().isEmpty()) {
                    		item.mPageName = map["pageName"].toString();
                		}
                		if (!map["category"].toString().trimmed().isEmpty()) {
                    		item.mCategory = map["category"].toString();
                		}

                		QVariant addCat = map["additionalCategories"];
                		if (addCat != 0 && addCat.type() == QVariant::String) {
                			QString acString = addCat.toString();
                			item.mAdditionalCategories.push_back(acString);
                		} else if (addCat != 0 && addCat.type() == QVariant::List) {
                			QList<QVariant> aclist = addCat.toList();
                			QList<QVariant>::iterator acit = aclist.begin();
                			for (; acit != aclist.end(); ++acit) {
                				QString acString = acit->toString();
                				item.mAdditionalCategories.push_back(acString);
                			}
                		}
                		item.mFloorId = map["floorId"].toString();
                		QVariant allFlo = map["allFloors"];
                		if (allFlo != 0 && allFlo.type() == QVariant::String) {
                			QString afInt = allFlo.toString();
                			item.mAllFloors.push_back(afInt);
                		} else if (allFlo != 0 && allFlo.type() == QVariant::List) {
                			QList<QVariant> aflist = allFlo.toList();
                			QList<QVariant>::iterator afit = aflist.begin();
                			for (; afit != aflist.end(); ++afit) {
                				QString afInt = afit->toString();
                				item.mAllFloors.push_back(afInt);
                			}
                		}
                		QVariant rooId = map["roomId"];
                		if (rooId != 0 && rooId.type() == QVariant::String) {
                			QString riInt = rooId.toString();
                			item.mRoomId.push_back(riInt);
                		} else if (rooId != 0 && rooId.type() == QVariant::List) {
                			QList<QVariant> rilist = rooId.toList();
                			QList<QVariant>::iterator riit = rilist.begin();
                			for (; riit != rilist.end(); ++riit) {
                				QString riInt = riit->toString();
                				item.mRoomId.push_back(riInt);
                			}
                		}
                		QMap<QString, QVariant> logoMap = map["logo"].toMap();
                		item.mLogoUrl = ApplicationUI::EMPORIA_API_URL() + logoMap["url"].toString();
                		mShopVector.push_back(item);
                	}
                }
            }
        } else {
        	mContext.activityDone("Network problem: " + reply->errorString());
        	reply->deleteLater();
            return;
        }
    }

    mContext.activityDone("Success");
    reply->deleteLater();
}

void NetworkPage1Butiker::loadShopFromServerWithId(const QString &id) {
	qDebug() << "PageBL1Butiker::loadShopFromServerWithId(" << id << ")";
	QUrl url(QString("http://api.emporia.se/api/shop/get/" + id));
    QNetworkRequest request(url);

    QNetworkAccessManager *networkAccessManager = new QNetworkAccessManager(this);
    networkAccessManager->get(request);
    connect(networkAccessManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(onGetReplyShopId(QNetworkReply*)));
}

void NetworkPage1Butiker::onGetReplyShopId(QNetworkReply *reply) {
	qDebug() << "NetworkPage1Butiker::onGetReplyShopId()";
    QString response;
    if (reply) {
        if (reply->error() == QNetworkReply::NoError) {
            const int available = reply->bytesAvailable();

            if (available > 0) {
                const QByteArray buffer(reply->readAll());
                JsonDataAccess ja;
                const QVariant jsonva = ja.loadFromBuffer(QString::fromUtf8(buffer));
                if (ja.hasError()) {
                    // Retrieve the error
                    DataAccessError theError = ja.error();

                    // Determine the type of error that occurred
                    if (theError.errorType() != DataAccessErrorType::None) {
                    	mContext.activityDone("JSon Parsing Error: " + ja.error().errorMessage());
                    	reply->deleteLater();
                    	return;
                    }
                } else {
                	BLShopDetailsItem item;
            		QMap<QString, QVariant> map = jsonva.toMap();

					item.mId = map["id"].toString();
					if (!map["pageName"].toString().trimmed().isEmpty()) {
						item.mPageName = map["pageName"].toString();
					}
					if (!map["category"].toString().trimmed().isEmpty()) {
						item.mCategory = map["category"].toString();
					}
					item.mFloorId = map["floorId"].toString();
					if (!map["accepsGiftCard"].toString().trimmed().isEmpty()) {
						item.mAcceptsGiftCard = map["accepsGiftCard"].toString();
					}
					if (!map["aboutText"].toString().trimmed().isEmpty()) {
						item.mAboutText = map["aboutText"].toString();
					}

					QMap<QString, QVariant> shopImageMap = map["shopImage"].toMap();
					item.mShopImageUrl = ApplicationUI::EMPORIA_API_URL() + shopImageMap["url"].toString();

					QMap<QString, QVariant> mapImageMap = map["mapImage"].toMap();
					item.mMapImageUrl = ApplicationUI::EMPORIA_API_URL() + mapImageMap["url"].toString();

					QMap<QString, QVariant> contactInfoMap = map["contactInfo"].toMap();
					if (!contactInfoMap["phone"].toString().trimmed().isEmpty()) {
						item.mContactInfoPhone = contactInfoMap["phone"].toString();
					}
					if (!contactInfoMap["website"].toString().trimmed().isEmpty()) {
						item.mContactInfoWebsite = contactInfoMap["website"].toString();
					}
					if (!contactInfoMap["email"].toString().trimmed().isEmpty()) {
						item.mContactInfoEmail = contactInfoMap["email"].toString();
					}
					if (!contactInfoMap["twitter"].toString().trimmed().isEmpty()) {
						item.mContactInfoTwitter = contactInfoMap["twitter"].toString();
					}
					if (!contactInfoMap["facebook"].toString().trimmed().isEmpty()) {
						item.mContactInfoFacebook = contactInfoMap["facebook"].toString();
					}

					QMap<QString, QVariant> logoMap = map["logo"].toMap();
					item.mLogoUrl = ApplicationUI::EMPORIA_API_URL() + logoMap["url"].toString();
                	mContext.pushShopIdPage(item);
                }
            }
        } else {
           	mContext.activityDone("Network problem: " + reply->errorString());
           	reply->deleteLater();
           	return;
        }
    }

   	mContext.activityDone("Success");
    reply->deleteLater();
}
