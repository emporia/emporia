/*
 * NetworkPage2Aktuellt.cpp
 *
 *  Created on: 5 mar 2013
 *      Author: eiteds
 */

#include "NetworkPage2Aktuellt.h"
#include "../BL/BLPage2Aktuellt.h"
#include "../applicationui.hpp"

#include <bb/data/DataAccessError>
#include <bb/data/JsonDataAccess>

#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QDebug>
#include <QUrl>

using namespace bb::data;

NetworkPage2Aktuellt::NetworkPage2Aktuellt(BLPage2Aktuellt &context) : mContext(context) {
	qDebug() << "NetworkPage2Aktuellt::NetworkPage2Aktuellt()";
}

NetworkPage2Aktuellt::~NetworkPage2Aktuellt() {
	qDebug() << "NetworkPage2Aktuellt::~NetworkPage2Aktuellt()";
	mNewsVector.clear();
}

std::vector<BLNewsItem> const& NetworkPage2Aktuellt::getNewsItemVector() {
	qDebug() << "NetworkPage2Aktuellt::getNewsItemVector()";
	return mNewsVector;
}

void NetworkPage2Aktuellt::loadNewsFromServer(int page) {
	qDebug() << "NetworkPage2Aktuellt::loadNewsFromServer(" << page << ")";
	QString urlText;
	urlText.sprintf("/api/news/latest/?p=%d&ps=10", page);
	QUrl url(ApplicationUI::EMPORIA_API_URL() + urlText);
	qDebug() << url.toString(QUrl::None);
    QNetworkRequest request(url);

    QNetworkAccessManager *networkAccessManager = new QNetworkAccessManager(this);
    networkAccessManager->get(request);
    connect(networkAccessManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(onGetReplyNewsList(QNetworkReply*)));
}

void NetworkPage2Aktuellt::onGetReplyNewsList(QNetworkReply* reply) {
	qDebug() << "NetworkPage2Aktuellt::onGetReplyNewsList()";
    QString response;
    if (reply) {
        if (reply->error() == QNetworkReply::NoError) {
            const int available = reply->bytesAvailable();

            if (available > 0) {
                const QByteArray buffer(reply->readAll());

                JsonDataAccess ja;
                const QVariant jsonva = ja.loadFromBuffer(QString::fromUtf8(buffer));

                if (ja.hasError()) {
                    // Retrieve the error
                    DataAccessError theError = ja.error();

                    // Determine the type of error that occurred
                    if (theError.errorType() != DataAccessErrorType::None) {
                    	mContext.activityDone("JSon Parsing Error: " + ja.error().errorMessage());
                    	reply->deleteLater();
                        return;
                    }
                } else {
                	QMap<QString, QVariant> map = jsonva.toMap();
                	QList<QVariant> itemlist = map["items"].toList();

            		QList<QVariant>::iterator itemit = itemlist.begin();
                	for (; itemit != itemlist.end(); ++itemit) {
                		QMap<QString, QVariant> itemMap = itemit->toMap();
                		BLNewsItem item;

                		item.mPageSize = map["pageSize"].toString();
                		item.mPage = map["page"].toString();
                		item.mStartIndex = map["startIndex"].toString();
                		item.mTotalCount = map["totalCount"].toString();

                		item.mId = itemMap["id"].toString();
                		item.mName = itemMap["name"].toString();

                		QMap<QString, QVariant> iconMap = itemMap["icon"].toMap();
                		item.mIconUrl = ApplicationUI::EMPORIA_API_URL() + iconMap["url"].toString();

                		item.mPublished = itemMap["published"].toString();
                		item.mMainIntro = itemMap["mainIntro"].toString();

                		mNewsVector.push_back(item);
                	}
                }
            }
        } else {
        	mContext.activityDone("Network problem: " + reply->errorString());
        	reply->deleteLater();
        	return;
        }
    }

    mContext.activityDone("Success");
    reply->deleteLater();
}

void NetworkPage2Aktuellt::loadNewsFromServerWithId (const QString &id) {
	QUrl url(ApplicationUI::EMPORIA_API_URL() + "/api/news/get/" + id);
    QNetworkRequest request(url);

    QNetworkAccessManager *networkAccessManager = new QNetworkAccessManager(this);
    networkAccessManager->get(request);
    connect(networkAccessManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(onGetReplyNewsId(QNetworkReply*)));
}

void NetworkPage2Aktuellt::onGetReplyNewsId(QNetworkReply* reply) {
	qDebug() << "NetworkPage2Aktuellt::onGetReplyNewsId()";
    QString response;
    if (reply) {
        if (reply->error() == QNetworkReply::NoError) {
            const int available = reply->bytesAvailable();

            if (available > 0) {
                const QByteArray buffer(reply->readAll());

                JsonDataAccess ja;
                const QVariant jsonva = ja.loadFromBuffer(QString::fromUtf8(buffer));

                if (ja.hasError()) {
                    // Retrieve the error
                    DataAccessError theError = ja.error();

                    // Determine the type of error that occurred
                    if (theError.errorType() != DataAccessErrorType::None) {
                    	mContext.activityDone("JSon Parsing Error: " + ja.error().errorMessage());
                    	reply->deleteLater();
                        return;
                    }
                } else {
					qDebug() << "Reading in response";
                	BLNewsDetailsItem sditem;
            		QMap<QString, QVariant> map = jsonva.toMap();

            		sditem.mId = map["id"].toString();
            		sditem.mName = map["name"].toString();
            		sditem.mMainBody = map["mainBody"].toString();
            		sditem.mMainIntro = map["mainIntro"].toString();

					QMap<QString, QVariant> sourceMap = map["source"].toMap();
					sditem.mSourceId = sourceMap["id"].toString();
					sditem.mSourceName = sourceMap["name"].toString();
					sditem.mSourceType = sourceMap["type"].toString();

					QMap<QString, QVariant> iconMap = map["icon"].toMap();
					sditem.mIconUrl = ApplicationUI::EMPORIA_API_URL() + iconMap["url"].toString();

					sditem.mPublished = map["published"].toString();
					mContext.pushNewsIdPage(sditem);
                }
            }
        } else {
        	mContext.activityDone("Network problem: " + reply->errorString());
           	reply->deleteLater();
            return;
        }
    }

	mContext.activityDone("Success");
    reply->deleteLater();
}
