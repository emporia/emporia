/*
 * NetworkPage1Butiker.h
 *
 *  Created on: 28 feb 2013
 *      Author: eiteds
 */

#ifndef NETWORKPAGE1BUTIKER_H_
#define NETWORKPAGE1BUTIKER_H_

#include <QtCore/QObject>

#include <vector>

class QNetworkReply;
class BLShopItem;
class BLPage1Butiker;

class NetworkPage1Butiker : public QObject {
	Q_OBJECT
public:
	NetworkPage1Butiker(BLPage1Butiker &context);
	~NetworkPage1Butiker();

	void loadShopsFromServer ();
	void loadShopFromServerWithId (const QString &id);

	std::vector<BLShopItem> const& getShopItemVector();

private Q_SLOTS:
    void onGetReplyShopList(QNetworkReply *reply);
    void onGetReplyShopId(QNetworkReply *reply);

private:
	std::vector<BLShopItem> mShopVector;

    BLPage1Butiker &mContext;
};

#endif /* NETWORKPAGE1BUTIKER_H_ */
