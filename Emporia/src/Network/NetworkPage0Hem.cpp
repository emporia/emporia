/*
 * NetworkPage0Hem.cpp
 *
 *  Created on: 28 feb 2013
 *      Author: eiteds
 */


#include "../BL/BLPage0Hem.h"
#include "../BL/BLSlideItem.h"
#include "../BL/BLNewsDetailsItem.h"
#include "../Utilities/StringUtils.h"

#include "../applicationui.hpp"

#include <bb/data/DataAccessError>
#include <bb/data/JsonDataAccess>

#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>

#include "NetworkPage0Hem.h"

using namespace bb::data;

NetworkPage0Hem::NetworkPage0Hem(BLPage0Hem &context) : mContext(context) {
	qDebug() << "NetworkPage0Hem::NetworkPage0Hem()";
}

NetworkPage0Hem::~NetworkPage0Hem() {
	qDebug() << "NetworkPage0Hem::~NetworkPage0Hem()";
	mSlideVector.clear();
}

vector<BLSlideItem> const& NetworkPage0Hem::getSlideItemVector() {
	qDebug() << "NetworkPage0Hem::getSlideItemVector()";
	return mSlideVector;
}

void NetworkPage0Hem::loadSlideShowFromServer() {
	qDebug() << "NetworkPage0Hem::loadSlideShowFromServer()";
	QUrl url(ApplicationUI::EMPORIA_API_URL() + "/api/slideshow/slides");
    QNetworkRequest request(url);

    QNetworkAccessManager *networkAccessManager = new QNetworkAccessManager(this);
    networkAccessManager->get(request);
    connect(networkAccessManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(onGetReplySlideList(QNetworkReply*)));
}

void NetworkPage0Hem::onGetReplySlideList(QNetworkReply *reply) {
	qDebug() << "NetworkPage0Hem::onGetReplySlideList()";
    QString response;
    if (reply) {
        if (reply->error() == QNetworkReply::NoError) {
            const int available = reply->bytesAvailable();

            if (available > 0) {
                const QByteArray buffer(reply->readAll());

                JsonDataAccess ja;
                const QVariant jsonva = ja.loadFromBuffer(QString::fromUtf8(buffer));

                if (ja.hasError()) {
                    // Retrieve the error
                    DataAccessError theError = ja.error();

                    // Determine the type of error that occurred
                    if (theError.errorType() != DataAccessErrorType::None) {
                    	mContext.activityDone("JSon Parsing Error: " + ja.error().errorMessage());
                    	reply->deleteLater();
                        return;
                    }
                } else {
                	QList<QVariant> itemlist = jsonva.toList();
                	QList<QVariant>::iterator itemit = itemlist.begin();
                	for (; itemit != itemlist.end(); ++itemit) {
                		QMap<QString, QVariant> map = itemit->toMap();
                		BLSlideItem item;

                		item.mTitle = map["title"].toString();
                		item.mSubtitle = map["subtitle"].toString();

                		QMap<QString, QVariant> imageMap = map["image"].toMap();
                		QString urlText = imageMap["url"].toString();
                		urlText.replace(QString("%c3%b6"), QString("ö"));
                		urlText.replace(QString("%c3%a4"), QString("ä"));
                		urlText.replace(QString("%c3%a5"), QString("å"));
                		item.mImageUrl = ApplicationUI::EMPORIA_BASE_URL() + urlText;

                		QMap<QString, QVariant> linkMap = map["link"].toMap();
                		item.mLinkId = linkMap["id"].toString();
                		item.mLinkName = linkMap["name"].toString();
                		item.mLinkType = linkMap["type"].toString();

                		mSlideVector.push_back(item);
                	}
                }
            }
        } else {
        	mContext.activityDone("Network problem: " + reply->errorString());
        	reply->deleteLater();
            return;
        }
    }

	qDebug() << mSlideVector.size() << "items in mSlideVector";
    mContext.activityDone("Success");
    reply->deleteLater();
}

void NetworkPage0Hem::loadNewsFromServerWithId (const QString &id) {
	qDebug() << "NetworkPage0Hem::loadNewsFromServerFromId(" << id << ")";
	QUrl url(ApplicationUI::EMPORIA_API_URL() + "/api/news/get/" + id);
    QNetworkRequest request(url);

    QNetworkAccessManager *networkAccessManager = new QNetworkAccessManager(this);
    networkAccessManager->get(request);
    connect(networkAccessManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(onGetReplyNewsId(QNetworkReply*)));
}

void NetworkPage0Hem::onGetReplyNewsId(QNetworkReply *reply) {
	qDebug() << "NetworkPage0Hem::onGetReplyNewsId()";
    QString response;
    if (reply) {
        if (reply->error() == QNetworkReply::NoError) {
            const int available = reply->bytesAvailable();

            if (available > 0) {
                const QByteArray buffer(reply->readAll());

                JsonDataAccess ja;
                const QVariant jsonva = ja.loadFromBuffer(QString::fromUtf8(buffer));

                if (ja.hasError()) {
                    // Retrieve the error
                    DataAccessError theError = ja.error();

                    // Determine the type of error that occurred
                    if (theError.errorType() != DataAccessErrorType::None) {
                       	mContext.activityDone("JSon Parsing Error: " + ja.error().errorMessage());
                    	reply->deleteLater();
                        return;
                    }
                } else {
					qDebug() << "Reading in response";
                	BLNewsDetailsItem sditem;
            		QMap<QString, QVariant> map = jsonva.toMap();

            		sditem.mId = map["id"].toString();
            		sditem.mName = map["name"].toString();
            		sditem.mMainBody = map["mainBody"].toString();
            		sditem.mMainIntro = map["mainIntro"].toString();

					QMap<QString, QVariant> sourceMap = map["source"].toMap();
					sditem.mSourceId = sourceMap["id"].toString();
					sditem.mSourceName = sourceMap["name"].toString();
					sditem.mSourceType = sourceMap["type"].toString();

					QMap<QString, QVariant> iconMap = map["icon"].toMap();
					sditem.mIconUrl = ApplicationUI::EMPORIA_API_URL() + iconMap["url"].toString();

					sditem.mPublished = map["published"].toString();
					mContext.pushSlideIdPage(sditem);
                }
            }
        } else {
           	mContext.activityDone("Network problem: " + reply->errorString());
           	reply->deleteLater();
            return;
        }
    }

   	mContext.activityDone("Success");
    reply->deleteLater();
}
