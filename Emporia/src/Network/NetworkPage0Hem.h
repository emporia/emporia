/*
 * NetworkPage0Hem.h
 *
 *  Created on: 28 feb 2013
 *      Author: eiteds
 */

#ifndef NETWORKPAGE0HEM_H_
#define NETWORKPAGE0HEM_H_

#include <QtCore/QObject>

#include <vector>

class QNetworkReply;
class BLPage0Hem;
class BLSlideItem;

using namespace std;

class NetworkPage0Hem : public QObject {
	Q_OBJECT
public:
	NetworkPage0Hem(BLPage0Hem &context);
	~NetworkPage0Hem();

    void loadSlideShowFromServer();
	void loadNewsFromServerWithId(const QString &id);

	std::vector<BLSlideItem> const& getSlideItemVector();

private Q_SLOTS:
    void onGetReplySlideList(QNetworkReply *reply);
    void onGetReplyNewsId(QNetworkReply *reply);

private:
    vector<BLSlideItem> mSlideVector;

    BLPage0Hem &mContext;
};

#endif /* NETWORKPAGE0HEM_H_ */
