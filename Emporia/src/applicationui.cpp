// Default empty project template
#include "applicationui.hpp"

#include "BL/BLPage3Oppettider.h"
#include "BL/BLPage2Aktuellt.h"
#include "BL/BLPage1Butiker.h"
#include "BL/BLPage0Hem.h"

#include <bb/cascades/Application>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/AbstractPane>
#include <bb/cascades/TabbedPane>
#include <bb/cascades/Container>
#include <bb/cascades/DockLayout>

#include <bb/system/SystemToast>

using namespace bb::cascades;
using namespace bb::system;

ApplicationUI::ApplicationUI(bb::cascades::Application *app)
: QObject(app)
{
	qDebug() << "ApplicationUI::ApplicationUI()";
    // create scene document from main.qml asset
    // set parent to created document to ensure it exists for the whole application lifetime
    QmlDocument *qml = QmlDocument::create("asset:///main.qml").parent(this);

    // create root object for the UI
    mRoot = qml->createRootObject<AbstractPane>();
    // set created root object as a scene

	connect(mRoot, SIGNAL(activeTabChanged(bb::cascades::Tab*)), this,
			SLOT(activityBegin(bb::cascades::Tab*)));
	connect(&mStatusEventHandler, SIGNAL(networkStatusUpdated(bool, QString)),
			this, SLOT(networkStatusUpdateHandler(bool, QString)));
	mIsInitialized = false;
	mIsNetworkActive = false;
	// init BL instances with zeros...
	for (int i = 0; i < 4; i++) {
		mBLPage[i] = 0;
		mInitMap.insert(std::make_pair(i, false));
	}
	mCurrentPage = 0;

	app->setScene(mRoot);
}

const bb::cascades::AbstractPane& ApplicationUI::getRoot() {
	return *mRoot;
}

void ApplicationUI::activityBegin(bb::cascades::Tab* activeTab) {
	qDebug() << "ApplicationUI::activityBegin(" << activeTab->objectName() << ") ----------------------------|> ";
	if ((activeTab->objectName() == "page0Tab" && mCurrentPage == 0)
		|| (activeTab->objectName() == "page1Tab" && mCurrentPage == 1)
		|| (activeTab->objectName() == "page2Tab" && mCurrentPage == 2)
		|| (activeTab->objectName() == "page3Tab" && mCurrentPage == 3)) {
		qDebug() << "Ignoring new request for page already loading...";
		return;
	}
	if (!mIsNetworkActive) {
		SystemToast *toast = new SystemToast(this);
		toast->setBody("Network connection problem, cannot load page content from the internet.\nPlease check your SIM-card or WIFI configuration and try again!");
		toast->show();
		return;
	}
	if (!mInitMap.find(mCurrentPage)->second) {
		qDebug() << "Current tab still not done loading, returning...";
		return;
	}
	if (mBLPage[mCurrentPage] != 0) {
		mBLPage[mCurrentPage]->beforeSwitch();
	}
	if (activeTab->objectName() == "page0Tab" && mCurrentPage != 0) {
		mCurrentPage = 0;
		if (mBLPage[mCurrentPage] == 0) {
			mBLPage[mCurrentPage] = new BLPage0Hem(*this);
			mBLPage[mCurrentPage]->initCall();
			mIsInitialized = true;
		}
	} else if (activeTab->objectName() == "page1Tab" && mCurrentPage != 1) {
		mCurrentPage = 1;
		if (mBLPage[mCurrentPage] == 0) {
			mBLPage[mCurrentPage] = new BLPage1Butiker(*this);
			mBLPage[mCurrentPage]->initCall();
			mIsInitialized = true;
		}
	} else if (activeTab->objectName() == "page2Tab" && mCurrentPage != 2) {
		mCurrentPage = 2;
		if (mBLPage[mCurrentPage] == 0) {
			mBLPage[mCurrentPage] = new BLPage2Aktuellt(*this);
			mBLPage[mCurrentPage]->initCall();
			mIsInitialized = true;
		}
	} else if (activeTab->objectName() == "page3Tab" && mCurrentPage != 3) {
		mCurrentPage = 3;
		if (mBLPage[mCurrentPage] == 0) {
			mBLPage[mCurrentPage] = new BLPage3Oppettider(*this);
			mBLPage[mCurrentPage]->initCall();
			mIsInitialized = true;
		}
	}
	if (mBLPage[mCurrentPage] != 0) {
		mBLPage[mCurrentPage]->afterSwitch();
	}
}

void ApplicationUI::activityDone(QString result) {
	qDebug() << "ApplicationUI::activityDone(" << result << ")";
	if (result != "Success") {
		SystemToast *toast = new SystemToast(this);
		toast->setBody(result); // Network or parsing problem...
		toast->show();
		return;
	}
	std::map<int, bool>::iterator it = mInitMap.find(mCurrentPage);
	if ((it != mInitMap.end()) && !it->second) {
		it->second = true;
		mBLPage[mCurrentPage]->initPage();
	} else {
		mBLPage[mCurrentPage]->showPage();
	}
}

void ApplicationUI::initPageInstances() {
	qDebug() << "ApplicationUI::initPageInstances()";
	if (mIsInitialized) {
		qDebug() << "Already initialized.";
		return;
	}
	// load startup functions
	if (mIsNetworkActive) {
		mBLPage[mCurrentPage] = new BLPage0Hem(*this);
		mBLPage[mCurrentPage]->initCall();
		mIsInitialized = true;
	}
}

void ApplicationUI::networkStatusUpdateHandler(bool status, QString type) {
	qDebug() << "ApplicationUI::networkStatusUpdateHandler(" << status << type << ")";
	if (status) {
		mIsNetworkActive = true;
		initPageInstances();
		return;
	} else if (!status && !type.compare("none")) {
		mIsNetworkActive = false;
		SystemToast *toast = new SystemToast(this);
		toast->setBody("Network connection problem, cannot load page content from the internet.\nPlease check your SIM-card or WIFI configuration and try again!");
		toast->show();
	}
}
