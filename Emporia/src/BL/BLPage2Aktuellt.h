/*
 * BLPage2Aktuellt.h
 *
 *  Created on: 5 mar 2013
 *      Author: eiteds
 */

#ifndef BLPAGE2AKTUELLT_H_
#define BLPAGE2AKTUELLT_H_

#include "BLPageBase.h"
#include "BLNewsItem.h"
#include "BLNewsDetailsItem.h"
#include "../Network/NetworkPage2Aktuellt.h"
#include "../applicationui.hpp"

#include <bb/cascades/Page>
#include <bb/cascades/NavigationPane>
#include <bb/cascades/ActivityIndicator>

#include <QtCore/QObject>

#include <vector>

using namespace bb::cascades;

class BLPage2Aktuellt: public QObject, public BLPageBase {
	Q_OBJECT
public:
	BLPage2Aktuellt(ApplicationUI &app);
	virtual ~BLPage2Aktuellt();

	virtual void initCall();
	virtual void initPage();
	virtual void showPage();
	virtual void beforeSwitch();
	virtual void afterSwitch();
	virtual void activityDone(QString result);

	virtual void pushNewsIdPage(const BLNewsDetailsItem &item);public Q_SLOTS:
	void triggered (QVariantList list);

private:
	void initDetailedPage(Container &container, const BLNewsDetailsItem &item);

	bool mIsLoading;
	int mCurrentPageIndex;
	int mPageSize;
	int mTotalCount;

	QScopedPointer<ActivityIndicator> mActivityIndicator;
	QScopedPointer<NavigationPane> mNavigationPane;
	QScopedPointer<Page> mDetailedPage;

	std::vector<BLNewsItem> mNewsVector;

	NetworkPage2Aktuellt mNetwork;
	ApplicationUI &mApp; // callback
};

#endif /* BLPAGE2AKTUELLT_H_ */
