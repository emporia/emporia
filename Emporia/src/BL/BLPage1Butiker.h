/*
 * BLPage1Butiker.h
 *
 *  Created on: 14 feb 2013
 *      Author: eiteds
 */

#ifndef BLPage1Butiker_H_
#define BLPage1Butiker_H_

#include "BLPageBase.h"
#include "BLShopItem.h"
#include "BLShopDetailsItem.h"
#include "../Network/NetworkPage1Butiker.h"
#include "../applicationui.hpp"

#include <bb/cascades/ImageView>
#include <bb/cascades/ActivityIndicator>
#include <bb/cascades/Page>
#include <bb/cascades/TouchEvent>
#include <bb/cascades/TextStyle>
#include <bb/cascades/SegmentedControl>
#include <bb/cascades/ScrollView>
#include <bb/cascades/Container>
#include <bb/cascades/Option>
#include <bb/cascades/NavigationPane>

#include <QtCore/QObject>

#include <vector>
#include <map>

using namespace bb::cascades;

class BLPage1Butiker: public QObject, public BLPageBase {
	Q_OBJECT
public:
	BLPage1Butiker(ApplicationUI &app);
	virtual ~BLPage1Butiker();

	virtual void initCall();
	virtual void initPage();
	virtual void showPage();
	virtual void beforeSwitch();
	virtual void afterSwitch();
	virtual void activityDone(QString result);

	void pushShopIdPage(const BLShopDetailsItem &item);

public Q_SLOTS:
	void selectedIndexChanged (int selectedIndex);
	void charHandleTouch(bb::cascades::TouchEvent *event);
	void categoryHandleTouch(bb::cascades::TouchEvent *event);
	void shopHandleTouch(bb::cascades::TouchEvent *event);
	void viewableAreaChanged(const QRectF &, float);
	void upArrowsHandleTouch(bb::cascades::TouchEvent *event);
	void fadeUpArrows();

private:

	void addShopItemToList(const BLShopItem &item, Container &container,
			int yPos);
	void initDetailedPage(Container &container, const BLShopDetailsItem &item);

	void createOption1Controls();
	void createOption2Controls();

	bool mIsLoading;

	QTimer mUpArrowsTimer;
	QScopedPointer<TextStyle> mOrangeStyle;
	QScopedPointer<TextStyle> mWhiteStyle;
	QScopedPointer<TextStyle> mGrayStyle;
	QScopedPointer<Page> mDetailedPage;
	QScopedPointer<SegmentedControl> mSegmentedControl;
	QScopedPointer<Container> mPageContainer;
	QScopedPointer<Container> mOption1Container;
	QScopedPointer<Container> mOption2Container;
	QScopedPointer<ScrollView> mScrollView;
	QScopedPointer<ImageView> mUpArrows;
	QScopedPointer<NavigationPane> mNavigationPane;
	QScopedPointer<ActivityIndicator> mActivityIndicator;

	std::vector<BLShopItem> mShopVector;
	std::vector<BLShopItem> mShopUnique;
	std::map<QString, int> mScrollMapPagename;
	std::map<QString, int> mScrollMapCategory;

	NetworkPage1Butiker mNetwork;
	ApplicationUI &mApp; // callback
};

#endif /* BLPage1Butiker_H_ */
