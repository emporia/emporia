/*
 * BLNewsDetailsItem.h
 *
 *  Created on: 5 mar 2013
 *      Author: eiteds
 */

#ifndef BLNEWSDETAILSITEM_H_
#define BLNEWSDETAILSITEM_H_

#include <QString>

class BLNewsDetailsItem {
public:
	BLNewsDetailsItem();
	virtual ~BLNewsDetailsItem();

	QString mMainBody;
	QString mSourceId;
	QString mSourceName;
	QString mSourceType;
	QString mId;
	QString mName;
	QString mMainIntro;
	QString mIconUrl;
	QString mPublished;
};

#endif /* BLNEWSDETAILSITEM_H_ */
