/*
 * BLPage3Oppettider.cpp
 *
 *  Created on: 5 mar 2013
 *      Author: eiteds
 */

#include "BLPage3Oppettider.h"

#include "../applicationui.hpp"

#include "../Network/NetworkPage3Oppettider.h"
#include "../Utilities/WebImageView.h"

#include <bb/cascades/AbsoluteLayout>
#include <bb/cascades/AbsoluteLayoutProperties>
#include <bb/cascades/ScrollView>
#include <bb/cascades/Label>
#include <bb/cascades/Divider>
#include <bb/cascades/Container>
#include <bb/cascades/SystemDefaults>
#include <bb/cascades/TextStyle>

#include <functional>
#include <algorithm>

using namespace bb::cascades;

BLPage3Oppettider::BLPage3Oppettider(ApplicationUI& app) :
		mNetwork(*this), mApp(app) {
	qDebug() << "BLPage3Oppettider::BLPage3Oppettider()";
}

BLPage3Oppettider::~BLPage3Oppettider() {
	qDebug() << "BLPage3Oppettider::~BLPage3Oppettider()";
	mIsLoading = false;
}

void BLPage3Oppettider::initCall() {
	qDebug() << "BLPage3Oppettider::initCall()";
	Container* c = mApp.getRoot().findChild<Container*>("page3Container");
	QScopedPointer<ActivityIndicator> temp(
			mApp.getRoot().findChild<ActivityIndicator*>("indicator"));
	mActivityIndicator.swap(temp);
	c->add(mActivityIndicator.data());
	mActivityIndicator->setVisible(true);
	mNetwork.loadOpeningHoursFromServer();
	mIsLoading = true;
}

class isOfCategory: public std::unary_function<BLOpeningHoursItem, bool> {
	QString mCategory;
public:
	isOfCategory(QString category) :
			mCategory(category) {
	}
	bool operator()(const BLOpeningHoursItem &obj) {
		return (!mCategory.compare(obj.mCategory) && obj.mIsPublished);
	}
};

struct FreqAscendingCategory {
	bool operator()(BLOpeningHoursItem const &lhs,
			BLOpeningHoursItem const &rhs) const {
		if (lhs.mCategory.compare(rhs.mCategory) < 0)
			return true;
		if (lhs.mCategory.compare(rhs.mCategory) > 0)
			return false;
		return false;
	}
};

void BLPage3Oppettider::initPage() {
	qDebug() << "BLPage3Oppettider::initPage()";

	mOpeningHoursVector = mNetwork.getOpeningHoursVector();
	qDebug() << mOpeningHoursVector.size() << " items in mOpeningHoursVector";

	std::vector<BLOpeningHoursItem> mUniqueCategoryOHVector;
	std::sort(mOpeningHoursVector.begin(), mOpeningHoursVector.end(),
			FreqAscendingCategory());
	std::copy(mOpeningHoursVector.begin(), mOpeningHoursVector.end(),
			std::back_inserter(mUniqueCategoryOHVector));
	mUniqueCategoryOHVector.erase(
			std::unique(mUniqueCategoryOHVector.begin(),
					mUniqueCategoryOHVector.end()),
			mUniqueCategoryOHVector.end());
	qDebug() << "size of mUniqueCategoryOHVector"
			<< mUniqueCategoryOHVector.size();

	Container *pageContainer = mApp.getRoot().findChild<Container*>(
			"page3Container");
	ScrollView *scrollView = ScrollView::create().scrollMode(
			ScrollMode::Vertical);
	scrollView->setLayoutProperties(new AbsoluteLayoutProperties());
	scrollView->setTranslationY(195.0);
	scrollView->setPreferredWidth(768.0);

	Container *container = Container::create();
	container->setHorizontalAlignment(HorizontalAlignment::Fill);
	container->setVerticalAlignment(VerticalAlignment::Fill);
	container->setLayout(new AbsoluteLayout());
	container->setPreferredWidth(768.0);

	TextStyle * whiteStyle = new TextStyle(
			SystemDefaults::TextStyles::bodyText());
	whiteStyle->setColor(Color::fromRGBA(1.0f, 0.5f, 0.0f, 1.0f));
	int yPos = 0;
	for (unsigned int i = 0; i < mUniqueCategoryOHVector.size(); i++) {
		// print headers
		Label *category = new Label();
		category->setText(mUniqueCategoryOHVector[i].mCategory.toUpper());
		category->textStyle()->setBase(*whiteStyle);
		category->setTranslation(25.0, yPos);

		Divider *divider = new Divider();
		divider->setPreferredWidth(768.0);
		divider->setTranslationY(yPos + 70.0);
		container->add(category);
		container->add(divider);
		yPos += 110.0;

		int amountOfElements = std::count_if(mOpeningHoursVector.begin(),
				mOpeningHoursVector.end(),
				isOfCategory(mUniqueCategoryOHVector[i].mCategory));

		Image buttonBGDef = Image("asset:///images/mobile-button-bg-def.png");
		ImageView *image = new ImageView();
		image->setImage(buttonBGDef);
		image->setPreferredSize(768.0, amountOfElements * 70.0);
		image->setTranslationY(yPos - 37.0);
		image->setOpacity(0.7);
		container->add(image);

		for (unsigned int y = 0; y < mOpeningHoursVector.size(); y++) {
			BLOpeningHoursItem item = mOpeningHoursVector[y];
			if (!mUniqueCategoryOHVector[i].mCategory.compare(item.mCategory)) {
				Label *name = new Label();
				name->setText(item.mName);
				name->setTranslation(25.0, yPos);
				Label *opened = new Label();
				opened->setText(item.mOpened);
				opened->setTranslation(450, yPos);
				container->add(name);
				container->add(opened);
				yPos += 70.0;
			}
		}
		yPos += 20.0;
	}

	scrollView->setContent(container);
	pageContainer->add(scrollView);
	scrollView->setPreferredHeight(yPos + 250.0);
	container->setPreferredHeight(yPos + 250.0);
	pageContainer->setPreferredHeight(yPos + 250.0);
	mApp.activityDone("Success");
}

void BLPage3Oppettider::showPage() {
	qDebug() << "BLPage3Oppettider::showPage()";
}

void BLPage3Oppettider::beforeSwitch() {
	qDebug() << "BLPage3Oppettider::beforeSwitch()";
}

void BLPage3Oppettider::afterSwitch() {
	qDebug() << "BLPage3Oppettider::afterSwitch()";
}

void BLPage3Oppettider::activityDone(QString result) {
	qDebug() << "BLPage3Oppettider::activityDone(" << result << ")";
	mActivityIndicator->setVisible(false);
	mApp.activityDone(result);
	mIsLoading = false;
}
