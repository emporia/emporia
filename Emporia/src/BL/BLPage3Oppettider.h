/*
 * BLPage3Oppettider.h
 *
 *  Created on: 5 mar 2013
 *      Author: eiteds
 */

#ifndef BLPAGE3OPPETTIDER_H_
#define BLPAGE3OPPETTIDER_H_

#include "BLPageBase.h"
#include "BLOpeningHoursItem.h"
#include "../Network/NetworkPage3Oppettider.h"
#include "../applicationui.hpp"

#include <bb/cascades/ActivityIndicator>

#include <QObject>

#include <vector>

using namespace bb::cascades;
using namespace std;

class BLPage3Oppettider : public QObject, public BLPageBase {
	Q_OBJECT
public:
	BLPage3Oppettider(ApplicationUI& app);
	virtual ~BLPage3Oppettider();

	virtual void initCall();
	virtual void initPage();
	virtual void showPage();
	virtual void beforeSwitch();
	virtual void afterSwitch();
	virtual void activityDone(QString result);

private:
    bool mIsLoading;

    QScopedPointer<ActivityIndicator> mActivityIndicator;

    std::vector<BLOpeningHoursItem> mOpeningHoursVector;

    NetworkPage3Oppettider mNetwork;
    ApplicationUI &mApp; // callback
};

#endif /* BLPAGE3OPPETTIDER_H_ */
