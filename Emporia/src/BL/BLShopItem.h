/*
 * BLShopItem.h
 *
 *  Created on: 14 feb 2013
 *      Author: eiteds
 */

#ifndef BLShopItem_H_
#define BLShopItem_H_

#include <QString>

#include <vector>

class BLShopItem {
public:
	BLShopItem();
	virtual ~BLShopItem();
	bool operator== (BLShopItem const &rhs) const {
		if (!mCategory.compare(rhs.mCategory)) {
			return true;
		}
		return false;
	}
	QString mId;
	QString mPageName;
	QString mCategory;
	std::vector<QString> mAdditionalCategories;
	QString mFloorId;
	std::vector<QString> mAllFloors;
	std::vector<QString> mRoomId;
	QString mLogoUrl;
	QString mLogoTimestamp; // not mapped
};

#endif /* BLShopItem_H_ */
