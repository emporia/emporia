/*
 * BLNewsItemLoader.h
 *
 *  Created on: 6 mar 2013
 *      Author: eiteds
 */

#ifndef BLNEWSITEMLOADER_H_
#define BLNEWSITEMLOADER_H_

#include <QtCore/QObject>

class BLNewsItemLoader : public QObject {
    Q_OBJECT

    Q_PROPERTY(QString name READ name NOTIFY newsChanged)
    Q_PROPERTY(QString published READ published NOTIFY newsChanged)
    Q_PROPERTY(QString imageUrl READ imageUrl NOTIFY newsChanged)
    Q_PROPERTY(QString mainIntro READ mainIntro NOTIFY newsChanged)
    Q_PROPERTY(QString reloadText READ reloadText NOTIFY newsChanged)

    QString name() const;
    QString published() const;
    QString imageUrl() const;
    QString mainIntro() const;
    QString reloadText() const;

public:
	BLNewsItemLoader(const QString reloadText);
	BLNewsItemLoader(const QString name, const QString published, const QString imageUrl, const QString mainIntro);
	virtual ~BLNewsItemLoader();

Q_SIGNALS:
	void newsChanged();

private:
	QString mName;
	QString mPublished;
	QString mImageUrl;
	QString mMainIntro;
	QString mReloadText;
};

#endif /* BLNEWSITEMLOADER_H_ */
