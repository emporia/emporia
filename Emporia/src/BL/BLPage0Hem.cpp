/*
 * BLPage0Hem.cpp
 *
 *  Created on: 18 feb 2013
 *      Author: eiteds
 */

#include "BLPage0Hem.h"

#include "../Utilities/QtObjectFormatter.hpp"
#include "../Utilities/StringUtils.h"
#include "../Utilities/WebImageView.h"

#include <bb/cascades/TapHandler>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/NavigationPaneProperties>
#include <bb/cascades/ActionItem>
#include <bb/cascades/TouchEnterEvent>
#include <bb/cascades/Label>
#include <bb/cascades/AbsoluteLayout>
#include <bb/cascades/AbsoluteLayoutProperties>
#include <bb/cascades/TextArea>
#include <bb/cascades/Color>
#include <bb/cascades/Container>
#include <bb/cascades/DockLayout>
#include <bb/cascades/ImageView>
#include <bb/cascades/Image>
#include <bb/cascades/LayoutOrientation>
#include <bb/cascades/ScalingMethod>
#include <bb/cascades/ScrollMode>
#include <bb/cascades/ScrollView>
#include <bb/cascades/ScrollViewProperties>
#include <bb/cascades/StackLayout>
#include <bb/cascades/TouchPropagationMode>
#include <bb/cascades/TextStyle>
#include <bb/cascades/HorizontalAlignment>
#include <bb/cascades/VerticalAlignment>
#include <bb/cascades/SystemDefaults>

#include <iostream>

using namespace bb::cascades;

BLPage0Hem::BLPage0Hem(ApplicationUI &app) : mNetwork(*this), mApp(app) {
	qDebug() << "BLPage0Hem::BLPage0Hem()";
	mCurrentSlideIndex = 0;
	mIsScrollingDone = true;
	mAutoForwardScrolling = true;
	mIsLoading = false;
}

BLPage0Hem::~BLPage0Hem() {
	qDebug() << "BLPage0Hem::~BLPage0Hem()";
}

void BLPage0Hem::initCall() {
	qDebug() << "BLPage0Hem::initCall()";
	Container* c = mApp.getRoot().findChild<Container*>("page0Container");
	QScopedPointer<ActivityIndicator> temp(mApp.getRoot().findChild<ActivityIndicator*>("indicator"));
	mActivityIndicator.swap(temp);
	c->add(mActivityIndicator.data());
	mActivityIndicator->start();
	resetTouchXVariables();
    mNetwork.loadSlideShowFromServer();
}

void BLPage0Hem::initPage() {
	qDebug() << "BLPage0Hem::initPage()";
    // Download images
	mSlideVector = mNetwork.getSlideItemVector();
	mActivityIndicator->setVisible(true);
    if (!mSlideVector.empty()) {
    	Container *pageContainer = mApp.getRoot().findChild<Container*>("page0Container");
    	pageContainer->setLayout(new DockLayout());
    	QScopedPointer<ScrollView> tempScrollView(ScrollView::create().scrollMode(ScrollMode::None));
    	mScrollView.swap(tempScrollView);
    	mScrollView->setLayoutProperties(new AbsoluteLayoutProperties());
    	mScrollView->setHorizontalAlignment(HorizontalAlignment::Fill);
    	mScrollView->setVerticalAlignment(VerticalAlignment::Fill);
    	mScrollView->setPreferredWidth(768.0);
    	mScrollView->setPreferredHeight(825.0);
    	mScrollView->setTranslationY(195.0);

    	Container *container = Container::create();
    	StackLayout *pStackLayout = new StackLayout();
    	pStackLayout->setOrientation(LayoutOrientation::LeftToRight);
    	container->setLayout(pStackLayout);
    	container->setPreferredWidth(768.0);
    	container->setPreferredHeight(1000.0);

    	TextStyle *blackBig = new TextStyle(SystemDefaults::TextStyles::bigText());
    	TextStyle *orangeBig = new TextStyle(SystemDefaults::TextStyles::bigText());
    	TextStyle *blackNormal = new TextStyle(SystemDefaults::TextStyles::titleText());
    	TextStyle *orangeNormal = new TextStyle(SystemDefaults::TextStyles::titleText());

    	blackBig->setColor(Color::Black);
    	blackBig->setFontSize(FontSize::XXLarge);
     	orangeBig->setColor(ApplicationUI::EMPORIA_COLOR_ORANGE());
    	orangeBig->setFontSize(FontSize::XXLarge);
    	blackNormal->setColor(Color::Black);
     	orangeNormal->setColor(ApplicationUI::EMPORIA_COLOR_ORANGE());
    	for (unsigned int i=0; i<mSlideVector.size(); i++) {
    		BLSlideItem item = mSlideVector[i];
    		Container *c = Container::create();
    		c->setLayout(new AbsoluteLayout());

    		WebImageView *view = new WebImageView(true, false);
    		view->setUrl(mSlideVector[i].mImageUrl);
    		view->setPreferredSize(768.0, 1000.0);
    		view->setOpacity(0.98);
    		view->setOverlapTouchPolicy(OverlapTouchPolicy::Allow);

    		Container *whitebox = Container::create().background(Color::White);
    		whitebox->setLayout(new AbsoluteLayout());
    		whitebox->setPreferredSize(700, 360);
    		whitebox->setTranslation(20, 510);
    		whitebox->setOpacity(0.95);
    		whitebox->setOverlapTouchPolicy(OverlapTouchPolicy::Allow);
    		Label *title = new Label;
    		title->setMultiline(true);
    		title->textStyle()->setBase(*orangeNormal);
    		title->setPreferredSize(650.0, 50);
    		title->setTranslation(40, 30);
    		title->setOverlapTouchPolicy(OverlapTouchPolicy::Allow);
    		QString titleText;
   			titleText = item.mTitle;
    		title->setText(titleText);
    		whitebox->add(title);
    		Label *subtitle = new Label;
    		QString subtitleText;
    		if (item.mLinkId.compare("24") != 0) {
    			if (item.mSubtitle.length() > 40) {
        			subtitleText = QString("<html>").append(item.mSubtitle).append(" <b>LÄS MER HÄR!</b></html>");
    			} else {
        			subtitleText = QString("<html>").append(item.mSubtitle).append("\n<b>LÄS MER HÄR!</b></html>");
    			}
        		TapHandler *pTapHandler = TapHandler::create().onTapped(this, SLOT(onTappedHandler(bb::cascades::TapEvent*)));
        		pageContainer->addGestureHandler(pTapHandler);
    		} else {
    			subtitleText = QString("<html>").append(item.mSubtitle).append("\n<b>KLICKA PÅ AKTUELLT!</b></html>");
    		}
    		if (item.mTitle.length() > 40) {
           		subtitle->setTranslation(40, 130);
    		} else {
           		subtitle->setTranslation(40, 80);
    		}
    		subtitle->setText(subtitleText);
    		subtitle->setMultiline(true);
            subtitle->textStyle()->setBase(*blackNormal);
    		subtitle->setPreferredSize(650, 200.0);
    		subtitle->setOverlapTouchPolicy(OverlapTouchPolicy::Allow);
    		whitebox->add(subtitle);
    		int xPos = 330;
    		for (unsigned int p=0; p<mSlideVector.size(); p++) {
    			if (i != p) {
    	    		Label *text = new Label();
    	    		text->setText(".");
    	    		text->textStyle()->setBase(*blackBig);
    	    		text->setTranslation(xPos, 230);
    	    		whitebox->add(text);
    			} else {
    	    		Label *text = new Label();
    	    		text->setText(".");
    	    		text->textStyle()->setBase(*orangeBig);
    	    		text->setTranslation(xPos, 230);
    	    		whitebox->add(text);
    			}
        		xPos += 20;
    		}

    		c->add(view);
    	    c->add(whitebox);
    		container->add(c);
    		mScrollView->setPreferredWidth(768.0*(i+1));
        	container->setPreferredWidth(768.0*(i+1));
    	}

    	QScopedPointer<NavigationPane> tempNavPane(mApp.getRoot().findChild<NavigationPane*>("tab0Nav"));
    	mNavigationPane.swap(tempNavPane);

    	mScrollView->setContent(container);
    	connect(mScrollView.data(), SIGNAL(touch(bb::cascades::TouchEvent*)), this, SLOT(scrollHandleTouch(bb::cascades::TouchEvent*)));
    	connect(mScrollView.data(), SIGNAL(viewableAreaChanged(const QRectF&, float)), this, SLOT(viewableAreaChanged(const QRectF&, float)));

		pageContainer->add(mScrollView.data());

    	connect(&mSlideTimer, SIGNAL(timeout()), this, SLOT(autoScrollSlides()));
    	mSlideTimer.start(5000);
    	mIsLoading = false;
    }
    mApp.activityDone("Success");
}

void BLPage0Hem::showPage() {
	qDebug() << "BLPage0Hem::showPage()";
}

void BLPage0Hem::beforeSwitch() {
	qDebug() << "BLPage0Hem::beforeSwitch()";
	mSlideTimer.stop();
}

void BLPage0Hem::afterSwitch() {
	qDebug() << "BLPage0Hem::afterSwitch()";
	mSlideTimer.start();
}

void BLPage0Hem::activityDone(QString result) {
	qDebug() << "BLPage0Hem::activityDone(" << result << ")";
	if (!result.contains("24 - server replied: Not Found")) {
		mApp.activityDone(result);
	}
	mActivityIndicator->setVisible(false);
	mIsLoading = false;
}

void BLPage0Hem::resetTouchXVariables() {
	mLastTouchXIndex = 0;
	for (int i=0; i<2; i++) {
		mTouchXArray[i] = 0;
	}
//	qDebug() << "---reset----";
}

void BLPage0Hem::scrollHandleTouch(bb::cascades::TouchEvent *event) {
	if (mSlideTimer.isActive()) {
		mSlideTimer.stop();
	}
	if (!mIsScrollingDone || mSlideVector.empty()) {
		return;
	}
	if (event->touchType() == TouchType::Move) {
		if (mLastTouchXIndex<2) {
			mTouchXArray[mLastTouchXIndex++] = event->localX();
			return;
		}
	} else if (event->touchType()==TouchType::Up) {
		resetTouchXVariables(); // confirm reset
		return;
	}
	if (mLastTouchXIndex==2) { // got 2 x reads, check drag direction
		if ((mTouchXArray[0] - mTouchXArray[mLastTouchXIndex-1]) >= 5) {
			if (mCurrentSlideIndex < (mSlideVector.size()-1)) {
				int point = ++mCurrentSlideIndex*768.0;
//				qDebug() << "slide" << mCurrentSlideIndex << "p" << point;
				mIsScrollingDone = false;
				mScrollView->scrollToPoint(point, 0, bb::cascades::ScrollAnimation::Smooth);
			}
		} else if ((mTouchXArray[mLastTouchXIndex-1] - mTouchXArray[0]) >= 5) {
			if (mCurrentSlideIndex > 0) {
				int point = --mCurrentSlideIndex*768.0;
//				qDebug() << "slide" << mCurrentSlideIndex << "p" << point;
				mIsScrollingDone = false;
				mScrollView->scrollToPoint(point, 0, bb::cascades::ScrollAnimation::Smooth);
			}
		}
		resetTouchXVariables(); // this action wasn't successful, has to be retried...
	}
}

void BLPage0Hem::viewableAreaChanged(const QRectF &/* viewableArea */, float /* contentScale */) {
	mIsScrollingDone = true;
}

void BLPage0Hem::autoScrollSlides() {
	if (mCurrentSlideIndex == 0) {
		mAutoForwardScrolling = true;
	} else if (mCurrentSlideIndex == (mSlideVector.size()-1)) {
		mAutoForwardScrolling = false;
	}
	if (mAutoForwardScrolling) {
		int point = ++mCurrentSlideIndex*768.0;
		mIsScrollingDone = false;
		mScrollView->scrollToPoint(point, 0, bb::cascades::ScrollAnimation::Smooth);
	} else {
		int point = --mCurrentSlideIndex*768.0;
		mIsScrollingDone = false;
		mScrollView->scrollToPoint(point, 0, bb::cascades::ScrollAnimation::Smooth);
	}
}

void BLPage0Hem::onTappedHandler(bb::cascades::TapEvent*) {
	if (!mIsLoading) {
		qDebug() << "BLPage1Butiker::onDoubleTappedHandler(" << mSlideVector[mCurrentSlideIndex].mLinkId << ")";
		if (!mSlideVector[mCurrentSlideIndex].mLinkId.compare("24")) {
			qDebug() << "No slide description, returning.";
			return;
		}
		if (mSlideTimer.isActive()) {
			mSlideTimer.stop();
		}
		QString objValue = mSlideVector[mCurrentSlideIndex].mLinkId;
		mNetwork.loadNewsFromServerWithId(objValue);
		mActivityIndicator->setOpacity(0.99);
		mActivityIndicator->setVisible(true);
		mIsLoading = true;
	}
}

void BLPage0Hem::pushSlideIdPage(const BLNewsDetailsItem &item) {
	qDebug() << "BLPage0Hem::pushNewsIdPage()";
	ActionItem* backAction = ActionItem::create().title("Bakåt").onTriggered(mNavigationPane.data(), SLOT(pop()));
	QmlDocument *qml = QmlDocument::create("asset:///qml/pageDetailedSlide.qml");
	Container *container = qml->createRootObject<Container>();
	container->setLayout(new DockLayout());
	initDetailedPage(*container, item);
	Label *label = container->findChild<Label*>("label1");
	QString labelText;
	if (item.mName.length() > 20) {
		labelText = item.mName.toUpper().mid(0,19).append("...");
	} else {
		labelText = item.mName.toUpper();
	}
	label->setText(labelText);
	QScopedPointer<Page> temp (Page::create().content(container).paneProperties(NavigationPaneProperties::create().backButton(backAction)));
	mDetailedPage.swap(temp);
	mNavigationPane->push(mDetailedPage.data());
}

void BLPage0Hem::initDetailedPage(Container &container, const BLNewsDetailsItem &item) {
	qDebug() << "BLPage0Hem::initDetailedPage()";
	ScrollView *scrollView = container.findChild<ScrollView*>("scrollView");
	Container *slideContainer = container.findChild<Container*>("slideContainer");
	WebImageView *logo = container.findChild<WebImageView*>("icon");
	logo->setUrl(item.mIconUrl);
	Label *headline = container.findChild<Label*>("headline");
	headline->setText(item.mName.toUpper());
	Label* published = container.findChild<Label*>("published");
	published->setText(item.mPublished);
	QString mainText;
	mainText.append("<html>").append(item.mMainIntro.trimmed()).append("\n");
	if (item.mId.compare("278") != 0) {
		mainText.append(item.mMainBody.trimmed()).append("</html>");
	} else {
		mainText.append("\n\n<p>Klicka på Butiker-taben nedan och se själv!</p></html>");
	}
	qDebug() << "mainText.length()" << mainText.length();
	if (mainText.length() > 1500) {
		container.setPreferredHeight(3300.0);
		scrollView->setPreferredHeight(3300.0);
		slideContainer->setPreferredHeight(3300.0);
	} else if (mainText.length() > 1000) {
		container.setPreferredHeight(2400.0);
		scrollView->setPreferredHeight(2400.0);
		slideContainer->setPreferredHeight(2400.0);
	} else if (mainText.length() > 500) {
		container.setPreferredHeight(1800.0);
		scrollView->setPreferredHeight(1800.0);
		slideContainer->setPreferredHeight(1800.0);
	} else {
		container.setPreferredHeight(1400.0);
		scrollView->setPreferredHeight(1400.0);
		slideContainer->setPreferredHeight(1400.0);
	}
	TextArea* main = container.findChild<TextArea*>("main");
	main->setText(mainText);
}
