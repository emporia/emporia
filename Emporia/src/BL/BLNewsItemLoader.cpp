/*
 * BLNewsItemLoader.cpp
 *
 *  Created on: 6 mar 2013
 *      Author: eiteds
 */

#include "BLNewsItemLoader.h"

BLNewsItemLoader::BLNewsItemLoader(const QString reloadText) {
	mReloadText  = reloadText;
}

BLNewsItemLoader::BLNewsItemLoader(const QString name, const QString published, const QString imageUrl, const QString mainIntro) {
	mName = name;
	mPublished = published;
	mImageUrl = imageUrl;
	mMainIntro = mainIntro;
}

BLNewsItemLoader::~BLNewsItemLoader() {
	// TODO Auto-generated destructor stub
}

QString BLNewsItemLoader::name() const {
	return mName;
}

QString BLNewsItemLoader::published() const {
	return mPublished;
}

QString BLNewsItemLoader::imageUrl() const {
	return mImageUrl;
}

QString BLNewsItemLoader::mainIntro() const {
	if (mMainIntro.length() >= 150) {
		return mMainIntro.mid(0,149).append("...");
	}
	return mMainIntro;
}

QString BLNewsItemLoader::reloadText() const {
	return mReloadText;
}
