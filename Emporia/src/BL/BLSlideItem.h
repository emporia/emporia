/*
 * BLSlideItem.h
 *
 *  Created on: 18 feb 2013
 *      Author: eiteds
 */

#ifndef BLSlideItem_H_
#define BLSlideItem_H_

#include <QString>

class BLSlideItem {
public:
	BLSlideItem();
	virtual ~BLSlideItem();

	QString mTitle;
	QString mSubtitle;
	QString mImageUrl;
	QString mImageTimestamp; // not mapped
	QString mLinkId;
	QString mLinkName;
	QString mLinkType;

};

#endif /* BLSlideItem_H_ */
