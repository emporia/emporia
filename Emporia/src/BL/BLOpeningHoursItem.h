/*
 * BLOpeningHoursItem.h
 *
 *  Created on: 7 mar 2013
 *      Author: eiteds
 */

#ifndef BLOPENINGHOURSITEM_H_
#define BLOPENINGHOURSITEM_H_

#include <QString>

class BLOpeningHoursItem {
public:
	BLOpeningHoursItem();
	virtual ~BLOpeningHoursItem();
	bool operator== (BLOpeningHoursItem const &rhs) const {
		if (!mCategory.compare(rhs.mCategory)) {
			return true;
		}
		return false;
	}

	QString mCategory;

	bool mIsOrdinary;
	bool mIsSpecial;

	QString mName;
	QString mOpened;

	bool mIsPublished;
	bool mIsSpecialVisible;
};

#endif /* BLOPENINGHOURSITEM_H_ */
