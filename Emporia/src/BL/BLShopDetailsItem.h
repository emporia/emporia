/*
 * BLShopDetailsItem.h
 *
 *  Created on: 27 feb 2013
 *      Author: eiteds
 */

#ifndef BLShopDetailsItem_H_
#define BLShopDetailsItem_H_

#include <QString>

class BLShopDetailsItem {
public:
	BLShopDetailsItem();
	virtual ~BLShopDetailsItem();

	QString mAboutText;
	QString mShopImageUrl;
	QString mMapImageUrl;
	QString mContactInfoPhone;
	QString mContactInfoWebsite;
	QString mContactInfoEmail;
	QString mContactInfoTwitter;
	QString mContactInfoFacebook;
	QString mId;
	QString mPageName;
	QString mCategory;
	QString mFloorId;
	QString mAcceptsGiftCard;
	QString mLogoUrl;
};

#endif /* BLShopDetailsItem_H_ */
