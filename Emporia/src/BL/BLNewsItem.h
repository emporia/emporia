/*
 * BLNewsItem.h
 *
 *  Created on: 5 mar 2013
 *      Author: eiteds
 */

#ifndef BLNEWSITEM_H_
#define BLNEWSITEM_H_

#include <QString>

class BLNewsItem {
public:
	BLNewsItem();
	virtual ~BLNewsItem();

	// global values
	QString mPageSize;
	QString mPage;
	QString mStartIndex;
	QString mTotalCount;

	// one news item
	QString mId;
	QString mName;
	QString mMainIntro;
	QString mIconUrl;
	QString mPublished;
};

#endif /* BLNEWSITEM_H_ */
