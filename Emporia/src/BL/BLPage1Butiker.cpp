/*
 * BLPage1Butiker.cpp
 *
 *  Created on: 14 feb 2013
 *      Author: eiteds
 */

#include "BLPage1Butiker.h"

#include "../Utilities/QtObjectFormatter.hpp"
#include "../Utilities/StringUtils.h"
#include "../Utilities/WebImageView.h"
#include "../Network/NetworkPage1Butiker.h"

#include <bb/cascades/ImageButton>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/ActionItem>
#include <bb/cascades/TextArea>
#include <bb/cascades/AbstractPane>
#include <bb/cascades/Button>
#include <bb/cascades/Container>
#include <bb/cascades/Color>
#include <bb/cascades/DockLayout>
#include <bb/cascades/AbsoluteLayout>
#include <bb/cascades/AbsoluteLayoutProperties>
#include <bb/cascades/NavigationPaneProperties>
#include <bb/cascades/Label>
#include <bb/cascades/ScrollView>
#include <bb/cascades/SegmentedControl>
#include <bb/cascades/SystemDefaults>

#include <iostream>
#include <algorithm>
#include <locale>
#include <set>
#include <functional>

using namespace bb::cascades;
using namespace std;

BLPage1Butiker::BLPage1Butiker(ApplicationUI &app) : mNetwork(*this), mApp(app) {
	qDebug() << "BLPage1Butiker::BLPage1Butiker()";
	mIsLoading = false;
}

BLPage1Butiker::~BLPage1Butiker() {
	qDebug() << "BLPage1Butiker::~BLPage1Butiker()";
}

void BLPage1Butiker::initCall() {
	qDebug() << "BLPage1Butiker::initCall()";
	Container* c = mApp.getRoot().findChild<Container*>("page1Container");
	QScopedPointer<ActivityIndicator> temp(
			mApp.getRoot().findChild<ActivityIndicator*>("indicator"));
	mActivityIndicator.swap(temp);
	c->add(mActivityIndicator.data());
	mActivityIndicator->setVisible(true);
	mNetwork.loadShopsFromServer();
}

struct FreqAscendingCategory {
	bool operator()(BLShopItem const &lhs, BLShopItem const &rhs) const {
		if (lhs.mCategory.compare(rhs.mCategory) < 0)
			return true;
		if (lhs.mCategory.compare(rhs.mCategory) > 0)
			return false;
		return false;
	}
};

void BLPage1Butiker::initPage() {
	qDebug() << "BLPage1Butiker::initPage()";
	mShopVector = mNetwork.getShopItemVector();
	if (!mShopVector.empty()) {
		Container *pageContainer = mApp.getRoot().findChild<Container*>(
				"page1Container");
		QScopedPointer<ScrollView> tempScrollView(
				mApp.getRoot().findChild<ScrollView*>("scrollView"));
		mScrollView.swap(tempScrollView);

		connect(mScrollView.data(),
				SIGNAL(viewableAreaChanged(const QRectF&, float)), this,
				SLOT(viewableAreaChanged(const QRectF&, float)));

		std::sort(mShopVector.begin(), mShopVector.end(),
				FreqAscendingCategory());
		std::copy(mShopVector.begin(), mShopVector.end(),
				std::back_inserter(mShopUnique));
		mShopUnique.erase(std::unique(mShopUnique.begin(), mShopUnique.end()),
				mShopUnique.end());
		qDebug() << "mShopVector" << mShopVector.size() << "mShopUnique"
				<< mShopUnique.size();

		QScopedPointer<TextStyle> tempTextStyle1(
				new TextStyle(SystemDefaults::TextStyles::bodyText()));
		mOrangeStyle.swap(tempTextStyle1);
		QScopedPointer<TextStyle> tempTextStyle2(
				new TextStyle(SystemDefaults::TextStyles::primaryText()));
		mWhiteStyle.swap(tempTextStyle2);
		QScopedPointer<TextStyle> tempTextStyle3(
				new TextStyle(SystemDefaults::TextStyles::bodyText()));
		mGrayStyle.swap(tempTextStyle3);

		QScopedPointer<SegmentedControl> tempSegmented(
				SegmentedControl::create());
		mSegmentedControl.swap(tempSegmented);
		Option *option1 = Option::create().text("Bokstavsordning").value(
				"option1").selected(true);
		Option *option2 = Option::create().text("Kategorier").value("option2");
		mSegmentedControl->add(option1);
		mSegmentedControl->add(option2);
		connect(mSegmentedControl.data(), SIGNAL(selectedIndexChanged (int)),
				this, SLOT(selectedIndexChanged (int)));

		QScopedPointer<Container> tempContainer(Container::create());
		mPageContainer.swap(tempContainer);
		mPageContainer->setHorizontalAlignment(HorizontalAlignment::Fill);
		mPageContainer->setVerticalAlignment(VerticalAlignment::Fill);
		mPageContainer->setLayout(new AbsoluteLayout());
		mPageContainer->setPreferredWidth(768.0);
		mPageContainer->setPreferredHeight(1060.0);
		mPageContainer->add(mSegmentedControl.data());

		createOption1Controls();
		mOption1Container->setVisible(true);

		createOption2Controls();
		mOption2Container->setVisible(false);

		mPageContainer->add(mOption1Container.data());
		mPageContainer->add(mOption2Container.data());
		mScrollView->setContent(mPageContainer.data());

		Image image = Image("asset:///images/up_arrows.png");
		QScopedPointer<ImageView> tempImageView(new ImageView());
		mUpArrows.swap(tempImageView);
		mUpArrows->setImage(image);
		mUpArrows->setPreferredSize(50.0, 50.0);
		mUpArrows->setTranslation(695.0, 0.0);
		mPageContainer->add(mUpArrows.data());
		mUpArrows->setVisible(false);
		connect(mUpArrows.data(), SIGNAL(touch(bb::cascades::TouchEvent*)),
				this, SLOT(upArrowsHandleTouch(bb::cascades::TouchEvent*)));

		QScopedPointer<NavigationPane> tempNavPane(
				mApp.getRoot().findChild<NavigationPane*>("tab1Nav"));
		mNavigationPane.swap(tempNavPane);

		mUpArrowsTimer.setSingleShot(true);
		connect(&mUpArrowsTimer, SIGNAL(timeout()), this, SLOT(fadeUpArrows()));

		selectedIndexChanged(0);
		pageContainer->add(mScrollView.data());
		mIsLoading = false;
	}
	mApp.activityDone("Success");
}

void BLPage1Butiker::showPage() {
	qDebug() << "BLPage1Butiker::showPage()";
}

void BLPage1Butiker::beforeSwitch() {
	qDebug() << "BLPage1Butiker::beforeSwitch()";
}

void BLPage1Butiker::afterSwitch() {
	qDebug() << "BLPage1Butiker::afterSwitch()";
}

void BLPage1Butiker::activityDone(QString result) {
	qDebug() << "BLPage1Butiker::activityDone(" << result << ")";
	mApp.activityDone(result);
	mActivityIndicator->setVisible(false);
	mIsLoading = false;
}

void BLPage1Butiker::selectedIndexChanged(int selectedIndex) {
	qDebug() << "BLPage1Butiker::selectedIndexChanged(" << selectedIndex << ")";
	if (!mIsLoading) {
		if (mPageContainer) {
			if (selectedIndex == 0) {
				mOption1Container->setVisible(true);
				mOption2Container->setVisible(false);
				mOption1Container->setPreferredHeight(25760.0);
				mPageContainer->setPreferredHeight(25760.0);
			} else if (selectedIndex == 1) {
				mOption1Container->setVisible(false);
				mOption2Container->setVisible(true);
				mOption2Container->setPreferredHeight(24895.0);
				mPageContainer->setPreferredHeight(24895.0);
			}
		}
	}
}

void BLPage1Butiker::createOption1Controls() {
	qDebug() << "BLPage1Butiker::createOption1Controls()";
	QScopedPointer<Container> temp(Container::create());
	mOption1Container.swap(temp);
	mOption1Container->setLayout(new AbsoluteLayout());
	mOption1Container->setHorizontalAlignment(HorizontalAlignment::Fill);
	mOption1Container->setVerticalAlignment(VerticalAlignment::Fill);
	mOption1Container->setPreferredWidth(768.0);
	mOption1Container->setTranslationY(100.0);
	QString alpha = "#ABCDEFGHIJKLMNOPQRSTUVWXYZÅÄÖ";
	int initX = 50, initY = 50;
	int colPos = initX, rowPos = initY, rowCount = 6;
	mShopVector = mNetwork.getShopItemVector();
	for (int i = 0; i < alpha.length(); i++) {
		QString asset = "asset:///alphabet/";

		bool found = false;
		for (unsigned int y = 0; y < mShopVector.size(); y++) {
			BLShopItem item = mShopVector[y];
			if (alpha[i] == L'#' && item.mPageName[0].isDigit()) {
				found = true;
				break;
			} else if (alpha[i] == item.mPageName[0]) {
				found = true;
				break;
			}
		}
		QString color = (found ? "w" : "b");
		if (alpha[i] == L'#') {
			asset.append("HS" + color + ".png");
		} else if (alpha[i] == L'Å') {
			asset.append("SO" + color + ".png");
		} else if (alpha[i] == L'Ä') {
			asset.append("SE" + color + ".png");
		} else if (alpha[i] == L'Ö') {
			asset.append("SU" + color + ".png");
		} else {
			asset.append(alpha[i] + color + ".png");
		}
		Image image = Image(asset);
		ImageView *view = new ImageView();
		view->setPreferredWidth(100);
		view->setPreferredHeight(100);
		view->setTranslationX(colPos);
		view->setTranslationY(rowPos);
		view->setImage(image);
		view->setObjectName("" + alpha[i]);
		connect(view, SIGNAL(touch(bb::cascades::TouchEvent*)), this,
				SLOT(charHandleTouch(bb::cascades::TouchEvent*)));

		colPos = ((i + 1) % rowCount) * 115 + initX;
		rowPos = ((i + 1) / rowCount) * 115 + initY;
		mOption1Container->add(view);
	}

	int yPos = rowPos + 200;
	mOrangeStyle->setColor(Color::fromRGBA(1.0f, 0.5f, 0.0f, 1.0f));
	mWhiteStyle->setColor(Color::fromRGBA(1.0f, 1.0f, 1.0f, 1.0f));
	mGrayStyle->setColor(Color::fromRGBA(1.0f, 0.8f, 0.8f, 0.8f));
	// create list of shops
	for (int i = 0; i < alpha.length(); i++) {
		Label *label = new Label();
		label->setText(QString(alpha[i]));
		label->setTranslationX(20.0);
		label->setTranslationY(yPos);
		label->textStyle()->setBase(*mOrangeStyle);
		yPos += 80.0;
		mOption1Container->add(label);

		for (unsigned int v = 0; v < mShopVector.size(); v++) {
			BLShopItem item = mShopVector[v];
			if (alpha[i] == L'#' && item.mPageName[0].isDigit()) {
				mScrollMapPagename.insert(
						std::pair<QString, int>(QString(alpha[i]), yPos));
				addShopItemToList(item, *mOption1Container.data(), yPos);
				yPos += 135.0;
			} else if (alpha[i] == item.mPageName[0]) {
				mScrollMapPagename.insert(
						std::pair<QString, int>(QString(alpha[i]), yPos));
				addShopItemToList(item, *mOption1Container.data(), yPos);
				yPos += 135.0;
			}
		}
		yPos += 20.0;
	}
	qDebug() << "createOption1Controls" << yPos;
}

void BLPage1Butiker::addShopItemToList(const BLShopItem &item,
		Container &container, int yPos) {
	Image buttonBGDef = Image("asset:///images/mobile-button-bg-def.png");
	Image buttonBGPre = Image("asset:///images/mobile-button-bg-pre.png");
	WebImageView *image = new WebImageView(false, true);
	image->setPreferredSize(100, 100);
	image->setOpacity(0.8);
	image->setOverlapTouchPolicy(OverlapTouchPolicy::Allow);
	image->setTranslationY(yPos+15.0);
	image->setTranslationX(25.0);
	image->setUrl(item.mLogoUrl);
	ImageButton *view = new ImageButton();
	view->setTranslationY(yPos);
	view->setDefaultImage(buttonBGDef);
	view->setPressedImage(buttonBGPre);
	view->setPreferredWidth(768.0);
	view->setOpacity(0.5);
	view->setObjectName(item.mId);
	connect(view, SIGNAL(touch(bb::cascades::TouchEvent*)), this,
			SLOT(shopHandleTouch(bb::cascades::TouchEvent*)));
	Label *pageName = new Label();
	if (item.mPageName.length() > 19) {
		QString pageText = item.mPageName.mid(0, 18).append("...");
		pageName->setText(pageText);
	} else {
		pageName->setText(item.mPageName);
	}
	pageName->setTranslationX(150.0);
	pageName->setTranslationY(yPos + 5.0);
	pageName->textStyle()->setBase(*mWhiteStyle);
	pageName->setOverlapTouchPolicy(OverlapTouchPolicy::Allow);
	Label *categoryName = new Label();
	categoryName->setText(item.mCategory);
	categoryName->setTranslationX(150.0);
	categoryName->setTranslationY(yPos + 60.0);
	categoryName->textStyle()->setBase(*mGrayStyle);
	categoryName->setOverlapTouchPolicy(OverlapTouchPolicy::Allow);
	Label *floorName = new Label();
	QString plan = QString("Plan ").append(item.mFloorId);
	floorName->setText(plan);
	floorName->setTranslationX(580.0);
	floorName->setTranslationY(yPos + 10.0);
	floorName->textStyle()->setBase(*mGrayStyle);
	floorName->setOverlapTouchPolicy(OverlapTouchPolicy::Allow);
	container.add(view);
	container.add(image);
	container.add(pageName);
	container.add(categoryName);
	container.add(floorName);
}

void BLPage1Butiker::createOption2Controls() {
	qDebug() << "BLPage1Butiker::createOption2Controls()";
	QScopedPointer<Container> temp(Container::create());
	mOption2Container.swap(temp);
	mOption2Container->setLayout(new DockLayout());
	mOption2Container->setHorizontalAlignment(HorizontalAlignment::Center);
	mOption2Container->setPreferredWidth(768.0);
	mOption2Container->setTranslationY(100.0);
	// create category buttons
	Image buttonBGDef = Image("asset:///images/mobile-button-bg-def.png");
	Image buttonBGPre = Image("asset:///images/mobile-button-bg-pre.png");
	int rowPos = 50;
	for (unsigned int i = 0; i < mShopUnique.size(); i++) {
		ImageButton *button = new ImageButton();
		button->setTranslationY(rowPos);
		button->setDefaultImage(buttonBGDef);
		button->setPressedImage(buttonBGPre);
		button->setPreferredWidth(768.0);
		button->setOpacity(0.5);

		Label *label = new Label();
		label->setText(mShopUnique[i].mCategory);
		label->setHorizontalAlignment(HorizontalAlignment::Center);
		label->setTranslationY(rowPos + 25.0);
		label->setOverlapTouchPolicy(OverlapTouchPolicy::Allow);

		rowPos += 115;
		button->setObjectName(mShopUnique[i].mCategory);
		connect(button, SIGNAL(touch(bb::cascades::TouchEvent*)), this,
				SLOT(categoryHandleTouch(bb::cascades::TouchEvent*)));
		mOption2Container->add(button);
		mOption2Container->add(label);
	}
	int yPos = rowPos + 115;
	mOrangeStyle->setColor(ApplicationUI::EMPORIA_COLOR_ORANGE());
	mWhiteStyle->setColor(Color::White);
	mGrayStyle->setColor(Color::LightGray);
	// create list of shops
	for (unsigned int i = 0; i < mShopUnique.size(); i++) {
		BLShopItem itemc = mShopUnique[i];
		Label *label = new Label();
		label->setText(itemc.mCategory.toUpper());
		label->setTranslationX(25.0);
		label->setTranslationY(yPos);
		label->textStyle()->setBase(*mOrangeStyle);
		yPos += 80.0;
		mOption2Container->add(label);
		for (unsigned int v = 0; v < mShopVector.size(); v++) {
			BLShopItem itemv = mShopVector[v];
			if (!itemc.mCategory.compare(itemv.mCategory)) {
				mScrollMapCategory.insert(
						std::pair<QString, int>(itemc.mCategory, yPos));
				addShopItemToList(itemv, *mOption2Container.data(), yPos);
				yPos += 135.0;
			}
		}
		yPos += 20.0;
	}
	qDebug() << "createOption2Controls" << yPos;
}

void BLPage1Butiker::charHandleTouch(bb::cascades::TouchEvent *event) {
	if (!mIsLoading && event->touchType() == TouchType::Up) {
		qDebug() << "BLPage1Butiker::charHandleTouch("
				<< event->target()->objectName().trimmed() << ")";
		QString objValue = event->target()->objectName().trimmed();
		std::map<QString, int>::iterator it = mScrollMapPagename.find(objValue);
		mScrollView->scrollToPoint(0, it->second,
				bb::cascades::ScrollAnimation::Smooth);
	}
}

void BLPage1Butiker::categoryHandleTouch(bb::cascades::TouchEvent *event) {
	if (!mIsLoading && event->touchType() == TouchType::Up) {
		qDebug() << "BLPage1Butiker::categoryHandleTouch("
				<< event->target()->objectName().trimmed() << ")";
		QString objValue = event->target()->objectName().trimmed();
		std::map<QString, int>::iterator it = mScrollMapCategory.find(objValue);
		mScrollView->scrollToPoint(0, it->second,
				bb::cascades::ScrollAnimation::Smooth);
	}
}

void BLPage1Butiker::shopHandleTouch(bb::cascades::TouchEvent *event) {
	if (!mIsLoading && event->touchType() == TouchType::Up) {
		qDebug() << "BLPage1Butiker::shopHandleTouch("
				<< event->target()->objectName().trimmed() << ")";
		mActivityIndicator->setVisible(true);
		QString objValue = event->target()->objectName().trimmed();
		mNetwork.loadShopFromServerWithId(objValue);
		mIsLoading = true;
	}
}

void BLPage1Butiker::pushShopIdPage(const BLShopDetailsItem &item) {
	qDebug() << "BLPage1Butiker::pushShopIdPage(" << item.mPageName << ")";
	ActionItem* backAction = ActionItem::create().title("Bakåt").onTriggered(
			mNavigationPane.data(), SLOT(pop()));
	QmlDocument *qml = QmlDocument::create("asset:///qml/pageDetailedShop.qml");
	Container *container = qml->createRootObject<Container>();
	container->setPreferredHeight(2000.0);
	container->setLayout(new DockLayout());
	initDetailedPage(*container, item);
	Label *label = container->findChild<Label*>("label1");
	if (item.mPageName.length() > 20) {
		QString pageText = item.mPageName.toUpper().mid(0, 19).append("...");
		label->setText(pageText);
	} else {
		label->setText(item.mPageName.toUpper());
	}
	QScopedPointer<Page> temp(
			Page::create().content(container).paneProperties(
					NavigationPaneProperties::create().backButton(backAction)));
	mDetailedPage.swap(temp);
	mNavigationPane->push(mDetailedPage.data());
}

void BLPage1Butiker::initDetailedPage(Container &container,
		const BLShopDetailsItem &item) {
	qDebug() << "BLPage1Butiker::initDetailedPage()";
	ScrollView *scrollView = container.findChild<ScrollView*>("scrollView");
	Container *shopContainer = container.findChild<Container*>("shopContainer");
	WebImageView *logo = container.findChild<WebImageView*>("logo");
	logo->setUrl(item.mLogoUrl);
	Label *headline = container.findChild<Label*>("headline");
	headline->setText(
			QString("Om ").toUpper().append(item.mPageName.toUpper()));
	TextArea *aboutText = container.findChild<TextArea*>("aboutText");
	aboutText->setText(item.mAboutText);
	TextArea *telefon = container.findChild<TextArea*>("telefon");
	if (item.mContactInfoPhone.contains("Information saknas.")) {
		telefon->setText(item.mContactInfoPhone);
	} else {
		QString telefonText = "<html><a href=\"tel:";
		telefonText.append(item.mContactInfoPhone).append("\">").append(
				item.mContactInfoPhone).append("</a></html>");
		telefon->setText(telefonText);
	}
	TextArea *website = container.findChild<TextArea*>("website");
	QString websiteText = "<html><a href=\"";
	if (item.mContactInfoWebsite.contains("http://")) {
		websiteText.append(item.mContactInfoWebsite).append("\">").append(
				item.mContactInfoWebsite).append("</a></html>");
		website->setText(websiteText);
	} else if (item.mContactInfoWebsite.contains("Information saknas.")) {
		website->setText(item.mContactInfoWebsite);
	} else {
		websiteText.append("http://").append(item.mContactInfoWebsite).append(
				"\">").append(item.mContactInfoWebsite).append("</a></html>");
		website->setText(websiteText);
	}
	WebImageView *map = container.findChild<WebImageView*>("map");
	qDebug() << item.mAboutText.length();
	map->setUrl(item.mMapImageUrl);
	if (item.mAboutText.length() > 1000) {
		container.setPreferredHeight(3300.0);
		scrollView->setPreferredHeight(3300.0);
		shopContainer->setPreferredHeight(3300.0);
	} else if (item.mAboutText.length() > 500) {
		container.setPreferredHeight(2800.0);
		scrollView->setPreferredHeight(2800.0);
		shopContainer->setPreferredHeight(2800.0);
	} else {
		container.setPreferredHeight(2500.0);
		scrollView->setPreferredHeight(2500.0);
		shopContainer->setPreferredHeight(2500.0);
	}
}

void BLPage1Butiker::viewableAreaChanged(const QRectF &viewableArea,
		float /* contentScale */) {
	if (viewableArea.top() > 880.0) {
		mUpArrows->setTranslation(695.0, viewableArea.top() + 20.0);
		mUpArrows->setVisible(true);
		mUpArrowsTimer.start(10000);
	} else {
		mUpArrows->setVisible(false);
	}
}

void BLPage1Butiker::upArrowsHandleTouch(bb::cascades::TouchEvent *event) {
	if (event->touchType() == TouchType::Up) {
		mUpArrows->setVisible(false);
		mScrollView->scrollToPoint(0.0, 0.0, ScrollAnimation::Smooth);
	}
}

void BLPage1Butiker::fadeUpArrows() {
	mUpArrows->setVisible(false);
}
