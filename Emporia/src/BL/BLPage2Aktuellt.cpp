/*
 * BLPage2Aktuellt.cpp
 *
 *  Created on: 5 mar 2013
 *      Author: eiteds
 */

#include "BLPage2Aktuellt.h"

#include "../applicationui.hpp"
#include "BLNewsDetailsItem.h"
#include "BLNewsItemLoader.h"

#include "../Network/NetworkPage2Aktuellt.h"
#include "../Utilities/WebImageView.h"

#include <bb/cascades/ScrollView>
#include <bb/cascades/DockLayout>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/TextArea>
#include <bb/cascades/Label>
#include <bb/cascades/NavigationPaneProperties>
#include <bb/cascades/ActionItem>
#include <bb/cascades/QListDataModel>
#include <bb/cascades/ListView>
#include <bb/cascades/Container>

using namespace bb::cascades;

BLPage2Aktuellt::BLPage2Aktuellt(ApplicationUI& app) :
		mNetwork(*this), mApp(app) {
	qDebug() << "BLPage2Aktuellt::BLPage2Aktuellt()";
	mCurrentPageIndex = 1;
	mTotalCount = 0;
	mPageSize = 10;
	mIsLoading = false;
}

BLPage2Aktuellt::~BLPage2Aktuellt() {
	qDebug() << "BLPage2Aktuellt::~BLPage2Aktuellt()";
}

void BLPage2Aktuellt::initCall() {
	qDebug() << "BLPage2Aktuellt::initCall()";
	Container* c = mApp.getRoot().findChild<Container*>("page2Container");
	QScopedPointer<ActivityIndicator> temp(
			mApp.getRoot().findChild<ActivityIndicator*>("indicator"));
	mActivityIndicator.swap(temp);
	mActivityIndicator->setVisible(true);
	c->add(mActivityIndicator.data());
	mNetwork.loadNewsFromServer(mCurrentPageIndex);
}

void BLPage2Aktuellt::initPage() {
	qDebug() << "BLPage2Aktuellt::initPage()";
	QScopedPointer<NavigationPane> tempNavPane(
			mApp.getRoot().findChild<NavigationPane*>("tab2Nav"));
	mNavigationPane.swap(tempNavPane);
	mActivityIndicator->setVisible(true);
	mApp.activityDone("Success");
}

void BLPage2Aktuellt::showPage() {
	qDebug() << "BLPage2Aktuellt::showPage()";
	mNewsVector = mNetwork.getNewsItemVector();
	qDebug() << mNewsVector.size() << "items in mNewsVector";

	if (!mNewsVector.empty()) { // reload
		qmlRegisterType<BLNewsItemLoader>();

		ListView *listView = mApp.getRoot().findChild<ListView*>("listView");
		QListDataModel<QObject*> *model = new QListDataModel<QObject*>();
		for (unsigned int i = 0; i < mNewsVector.size(); i++) {
			BLNewsItem item = mNewsVector[i];
			mTotalCount = item.mTotalCount.toInt();
			model->append(
					new BLNewsItemLoader(item.mName, item.mPublished,
							item.mIconUrl, item.mMainIntro));
		}
		QString reloadText;
		reloadText.sprintf("Läs in äldre nyheter (%d/%d)", mCurrentPageIndex,
				(mTotalCount / 10) + 1);
		model->append(new BLNewsItemLoader(reloadText.toUpper()));
		listView->setDataModel(model);
		connect(listView, SIGNAL(triggered(QVariantList)), this,
				SLOT(triggered (QVariantList)));
		mIsLoading = false;
	}
}

void BLPage2Aktuellt::beforeSwitch() {
	qDebug() << "BLPage2Aktuellt::beforeSwitch()";
}

void BLPage2Aktuellt::afterSwitch() {
	qDebug() << "BLPage2Aktuellt::afterSwitch()";
}

void BLPage2Aktuellt::activityDone(QString result) {
	qDebug() << "BLPage2Aktuellt::activityDone(" << result << ")";
	mApp.activityDone(result);
	mActivityIndicator->setVisible(false);
	mIsLoading = false;
}

void BLPage2Aktuellt::triggered(QVariantList list) {
	if (!mIsLoading && !list.empty()) {
		int newsIndex = list.begin()->toInt();
		int size = mNewsVector.size();
		if (newsIndex < size) {
			mActivityIndicator->setVisible(true);
			mNetwork.loadNewsFromServerWithId(mNewsVector[newsIndex].mId);
			mIsLoading = true;
		} else if (size < mTotalCount) {
			mActivityIndicator->setVisible(true);
			mNetwork.loadNewsFromServer(++mCurrentPageIndex);
			mIsLoading = true;
		}
	}
}

void BLPage2Aktuellt::pushNewsIdPage(const BLNewsDetailsItem &item) {
	qDebug() << "BLPage2Aktuellt::pushNewsIdPage()";
	ActionItem* backAction = ActionItem::create().title("Bakåt").onTriggered(
			mNavigationPane.data(), SLOT(pop()));
	QmlDocument *qml = QmlDocument::create(
			"asset:///qml/pageDetailedSlide.qml");
	Container *container = qml->createRootObject<Container>();
	container->setPreferredHeight(1500.0);
	container->setLayout(new DockLayout());
	initDetailedPage(*container, item);
	Label *label = container->findChild<Label*>("label1");
	QString labelText;
	if (item.mName.length() > 25) {
		labelText = item.mName.mid(0, 24).append("...");
	} else {
		labelText = item.mName;
	}
	label->setText(labelText);
	QScopedPointer<Page> temp(
			Page::create().content(container).paneProperties(
					NavigationPaneProperties::create().backButton(backAction)));
	mDetailedPage.swap(temp);
	mNavigationPane->push(mDetailedPage.data());
}

void BLPage2Aktuellt::initDetailedPage(Container &container,
		const BLNewsDetailsItem &item) {
	qDebug() << "BLPage2Aktuellt::initDetailedPage()";
	ScrollView *scrollView = container.findChild<ScrollView*>("scrollView");
	Container *slideContainer = container.findChild<Container*>(
			"slideContainer");
	WebImageView *logo = container.findChild<WebImageView*>("icon");
	logo->setUrl(item.mIconUrl);
	Label *headline = container.findChild<Label*>("headline");
	headline->setText(item.mName.toUpper());
	Label* published = container.findChild<Label*>("published");
	published->setText(item.mPublished);
	QString mainText;
	mainText.append("<html>").append(item.mMainIntro.trimmed()).append("\n");
	if (item.mId.compare("278") != 0) {
		mainText.append(item.mMainBody.trimmed()).append("</html>");
	} else {
		mainText.append(
				"\n\n<p>Klicka på Butiker-taben nedan och se själv!</p></html>");
	}
//		qDebug() << mainText.length();
	if (mainText.length() > 1500) {
		container.setPreferredHeight(3000.0);
		scrollView->setPreferredHeight(3000.0);
		slideContainer->setPreferredHeight(3000.0);
	} else if (mainText.length() > 800) {
		container.setPreferredHeight(2000.0);
		scrollView->setPreferredHeight(2000.0);
		slideContainer->setPreferredHeight(2000.0);
	} else if (mainText.length() > 400) {
		container.setPreferredHeight(1500.0);
		scrollView->setPreferredHeight(1500.0);
		slideContainer->setPreferredHeight(1500.0);
	} else {
		container.setPreferredHeight(1000.0);
		scrollView->setPreferredHeight(1000.0);
		slideContainer->setPreferredHeight(1000.0);
	}
	TextArea* main = container.findChild<TextArea*>("main");
	main->setText(mainText);
}
