/*
 * BLPageBase.h
 *
 *  Created on: 16 feb 2013
 *      Author: eiteds
 */

#ifndef BLPageBase_H_
#define BLPageBase_H_

#include <QString>

class BLPageBase {
public:
	BLPageBase();
	virtual ~BLPageBase();

	virtual void initCall() = 0; // network connection JSON
	virtual void initPage() = 0; // setup page from received content
	virtual void beforeSwitch() = 0; // before tab switch - stop timer?
	virtual void afterSwitch() = 0; // after tab switch - start timer?
	virtual void showPage() = 0; // show page on switch
	virtual void activityDone(QString result) = 0; // return from initCall; network calls (shown in systemtoast)
};

#endif /* BLPageBase_H_ */
