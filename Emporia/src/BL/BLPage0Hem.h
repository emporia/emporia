/*
 * BLPage0Hem.h
 *
 *  Created on: 18 feb 2013
 *      Author: eiteds
 */

#ifndef BLPage0Hem_H_
#define BLPage0Hem_H_

#include "../applicationui.hpp"

#include "BLPageBase.h"

#include "../Utilities/WebImageView.h"
#include "../Network/NetworkPage0Hem.h"
#include "BLSlideItem.h"
#include "BLNewsDetailsItem.h"

#include <bb/cascades/TapEvent>
#include <bb/cascades/Page>
#include <bb/cascades/Button>
#include <bb/cascades/ActivityIndicator>
#include <bb/cascades/ScrollView>
#include <bb/cascades/NavigationPane>

#include <QtCore/QObject>

#include <vector>

using namespace std;

class NetworkPage0Hem;
class ApplicationUI;

using namespace bb::cascades;

class BLPage0Hem: public QObject, public BLPageBase {
Q_OBJECT
public:
	BLPage0Hem(ApplicationUI &app);
	virtual ~BLPage0Hem();

	virtual void initCall();
	virtual void initPage();
	virtual void showPage();
	virtual void beforeSwitch();
	virtual void afterSwitch();
	virtual void activityDone(QString result);

	virtual void pushSlideIdPage(const BLNewsDetailsItem &item);

public Q_SLOTS:
	void scrollHandleTouch(bb::cascades::TouchEvent *event);
	void viewableAreaChanged(const QRectF &viewableArea, float contentScale);
	void autoScrollSlides();
	void onTappedHandler(bb::cascades::TapEvent*);

private:
	void initDetailedPage(Container &container, const BLNewsDetailsItem &item);
	void resetTouchXVariables();

	bool mIsLoading;
	bool mAutoForwardScrolling;
	bool mIsScrollingDone;
	int mLastTouchXIndex;
	int mTouchXArray[2];

	int mCurrentSlideIndex;

	vector<WebImageView> mImageVector;
	vector<BLSlideItem> mSlideVector;

	QTimer mSlideTimer;
	QScopedPointer<ActivityIndicator> mActivityIndicator;
	QScopedPointer<NavigationPane> mNavigationPane;
	QScopedPointer<Page> mDetailedPage;
	QScopedPointer<ScrollView> mScrollView;

	NetworkPage0Hem mNetwork;
	ApplicationUI &mApp; // callback
};

#endif /* BLPage0Hem_H_ */
