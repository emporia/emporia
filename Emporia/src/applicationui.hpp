// Default empty project template
#ifndef ApplicationUI_HPP_
#define ApplicationUI_HPP_

#include "BL/BLPageBase.h"
#include "Utilities/StatusEventHandler.h"

#include <bb/cascades/AbstractPane>
#include <bb/cascades/Tab>
#include <bb/cascades/Color>

#include <QObject>

#include <map>

namespace bb { namespace cascades { class Application; }}

/*!
 * @brief Application pane object
 *
 *Use this object to create and init app UI, to create context objects, to register the new meta types etc.
 */
class ApplicationUI : public QObject
{
    Q_OBJECT
public:
	static const QString EMPORIA_BASE_URL() {
		return QString("http://www.emporia.se");
	};
	static const QString EMPORIA_API_URL() {
		return QString ("http://api.emporia.se");
	};
	static const bb::cascades::Color EMPORIA_COLOR_ORANGE() {
		return bb::cascades::Color::fromRGBA(1.0f, 0.62f, 0.3f, 0.88f);
	}
	ApplicationUI(bb::cascades::Application *app);
    virtual ~ApplicationUI() {}

    void activityDone(QString result);
    const bb::cascades::AbstractPane& getRoot(); // get hold of user interface controls

public slots:
    void activityBegin(bb::cascades::Tab* activeTab);
    void networkStatusUpdateHandler(bool status, QString /*type*/);

private:
    bool mIsInitialized;
    bool mIsNetworkActive;
    StatusEventHandler mStatusEventHandler;

    int mCurrentPage;
    BLPageBase *mBLPage[4]; // contents of the 6 contents in the user interface
    bb::cascades::AbstractPane *mRoot;

    std::map<int, bool> mInitMap;
    void initPageInstances();
};


#endif /* ApplicationUI_HPP_ */
