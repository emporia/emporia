APP_NAME = Emporia

CONFIG += qt warn_on cascades10

include(config.pri)

QT += core network

INCLUDEPATH += ../src ${QNX_TARGET}/usr/include/bb/data ${QNX_TARGET}/usr/include/qt4/QtCore ${QNX_TARGET}/usr/include/qt4/QtNetwork
 
DEPENDPATH += ../src ${QNX_TARGET}/usr/include/bb/data  ${QNX_TARGET}/usr/include/qt4/QtCore ${QNX_TARGET}/usr/include/qt4/QtNetwork

LIBS += -lbbdata -lbbsystem -lbb
