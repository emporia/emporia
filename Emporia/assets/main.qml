// Default empty project template
import bb.cascades 1.00

TabbedPane {
    objectName: "tabbedPane1"
    showTabsOnActionBar: true
    peekEnabled: false
    sidebarState: SidebarState.Hidden
    Tab {
        objectName: "page0Tab"
        title: "HEM"
        imageSource: "asset:///dockicons/tab0w.png"
        NavigationPane {
            objectName: "tab0Nav"
            backButtonsVisible: true
            peekEnabled: true
            paneProperties: NavigationPaneProperties {
            }
		    Page {
		        ControlDelegate {
		            id: controlDelegate0
		            source: "qml/page0Hem.qml"
		            delegateActive: true
		        }
		    }
		}
    }
    Tab {
        objectName: "page1Tab"
        title: "BUTIKER"
        imageSource: "asset:///dockicons/tab1w.png"
        NavigationPane {
            objectName: "tab1Nav"
            backButtonsVisible: true
            peekEnabled: true
            paneProperties: NavigationPaneProperties {
            }
	        Page {
	            ControlDelegate {
	                id: controlDelegate1
	                source: "qml/page1Butiker.qml"
	                delegateActive: true 
	            }
	        }
        }
    }
    Tab {
        objectName: "page2Tab"
        title: "AKTUELLT"
        imageSource: "asset:///dockicons/tab2w.png"
        NavigationPane {
            objectName: "tab2Nav"
            backButtonsVisible: true
            peekEnabled: true
            paneProperties: NavigationPaneProperties {
            }
	        Page {
	            ControlDelegate {
	                id: controlDelegate2
	                source: "qml/page2Aktuellt.qml"
	                delegateActive: true
	            }
	        }
	    }
    }
    Tab {
        objectName: "page3Tab"
        title: "ÖPPETTIDER"
        imageSource: "asset:///dockicons/tab3w.png"
        NavigationPane {
            objectName: "tab3Nav"
            backButtonsVisible: true
            peekEnabled: true
            paneProperties: NavigationPaneProperties {
            }
	        Page {
	            ControlDelegate {
	                id: controlDelegate3
	                source: "qml/page3Oppettider.qml"
	                delegateActive: true
	            }
	        }
	    }
    }
    attachedObjects: [
        ActivityIndicator {
        	objectName: "indicator"
            preferredWidth: 100.0
            preferredHeight: 100.0
            translationX: 350.0
            translationY: 500.0
            opacity: 0.99
        }

    ]
}
	