// Default empty project template
import bb.cascades 1.00
import org.labsquare 1.0

Container {
    objectName: "pageContainer"
    property string headlineString: "Inte namngivet"
    Container {
	    layout: AbsoluteLayout {
	    }
	    ImageView {
	        id: imageView1
	        imageSource: "asset:///images/mobile-background.png"
	    }
	    ImageView {
	        id: imageView2
	        imageSource: "asset:///images/header-nav-bg.png"
	        horizontalAlignment: HorizontalAlignment.Fill
	        translationY: 34.0
	        scaleY: 2.60
	        maxWidth: 0.0
	        minWidth: 768.0
	    }
	    ImageView {
	        id: imageView3
	        imageSource: "asset:///images/emporia-logo.png"
	        scalingMethod: ScalingMethod.AspectFit
	        translationX: 60.0
	        translationY: 34.0
	        scaleX: 1.5
	        scaleY: 1.5
	    }
	    Label {
	        objectName: "label1"
	        text: headlineString
	        textStyle {
	            base: SystemDefaults.TextStyles.BodyText
	            color: Color.create(1, 0.5, 0.0)
	            fontStyle: FontStyle.Normal
	        }
	        translationX: 250.0
	        translationY: 25.0
	    }
	   Label {
	        id: label2
	        text: "MÅN - SÖN:"
	        textStyle {
	            base: SystemDefaults.TextStyles.SmallText
	            color: Color.create(1, 1, 0.8)
	            fontStyle: FontStyle.Normal
	        }
	        translationX: 250.0
	        translationY: 110.0
	    }
	   Label {
	        id: label3
	        text: "10 - 20"
	        textStyle {
	            base: SystemDefaults.TextStyles.SmallText
	            color: Color.create(1, 0.5, 0.0)
	            fontStyle: FontStyle.Normal
	        }
	        translationX: 380.0
	        translationY: 110.0
	    }
    }
    ScrollView {
    	objectName: "scrollView"
        scrollViewProperties.scrollMode: ScrollMode.Vertical
        layoutProperties: AbsoluteLayoutProperties {
        }
        preferredWidth: 768.0
        translationY: 200.0
        Container {
        	objectName: "slideContainer"
            verticalAlignment: VerticalAlignment.Fill
            horizontalAlignment: HorizontalAlignment.Fill
            preferredWidth: 768.0
            layout: AbsoluteLayout {
            }
            Label {
                objectName: "headline"
                textStyle.color: Color.create("#ffff7c00")
                translationX: 25.0
                translationY: 0.0
                preferredWidth: 700.0
                preferredHeight: 100.0
                multiline: true
            }
            Divider {
                preferredWidth: 768.0
                translationY: 120.0
            }
            Label {
                objectName: "published"
                textStyle.color: Color.Gray
                textStyle.fontSize: FontSize.Small
                textStyle.fontWeight: FontWeight.W200
                horizontalAlignment: HorizontalAlignment.Left
                textStyle.fontStyle: FontStyle.Italic
                translationX: 25.0
                translationY: 140.0
            }
            WebImageView {
                objectName: "icon"
                preferredWidth: 300.0
                preferredHeight: 400.0
                translationX: 420.0
                translationY: 140.0
                scaletofit: true
            }
            TextArea {
		        objectName: "main"
                textStyle.color: Color.White
                backgroundVisible: false
                inputMode: TextAreaInputMode.Chat
                editable: false
                input.submitKey: SubmitKey.None
                translationY: 520.0
                textStyle.fontSize: FontSize.Small
                preferredWidth: 750.0
                textStyle.textAlign: TextAlign.Justify
            }
        }
    }
}
