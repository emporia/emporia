// Default empty project template
import bb.cascades 1.00
import org.labsquare 1.0

Container {
    objectName: "pageContainer"
    property string headlineString: "Inte namngivet"
    Container {
        layout: AbsoluteLayout {
	    }
	    layoutProperties: AbsoluteLayoutProperties {
	    }
	    ImageView {
	        id: imageView1
	        imageSource: "asset:///images/mobile-background.png"
	    }
	    ImageView {
	        id: imageView2
	        imageSource: "asset:///images/header-nav-bg.png"
	        horizontalAlignment: HorizontalAlignment.Fill
	        translationY: 34.0
	        scaleY: 2.60
	        maxWidth: 0.0
	        minWidth: 768.0
	    }
	    ImageView {
	        id: imageView3
	        imageSource: "asset:///images/emporia-logo.png"
	        scalingMethod: ScalingMethod.AspectFit
	        translationX: 60.0
	        translationY: 34.0
            scaleX: 1.5
            scaleY: 1.5
        }
	    Label {
	        objectName: "label1"
	        text: headlineString
	        textStyle {
	            base: SystemDefaults.TextStyles.BodyText
	            color: Color.create(1, 0.5, 0.0)
	            fontStyle: FontStyle.Normal
	        }
	        translationX: 250.0
	        translationY: 25.0
	    }
	   Label {
	        id: label2
	        text: "MÅN - SÖN:"
	        textStyle {
	            base: SystemDefaults.TextStyles.SmallText
	            color: Color.create(1, 1, 0.8)
	            fontStyle: FontStyle.Normal
	        }
	        translationX: 250.0
	        translationY: 110.0
	    }
	   Label {
	        id: label3
	        text: "10 - 20"
	        textStyle {
	            base: SystemDefaults.TextStyles.SmallText
	            color: Color.create(1, 0.5, 0.0)
	            fontStyle: FontStyle.Normal
	        }
	        translationX: 380.0
	        translationY: 110.0
	    }
	}
    ScrollView {
        objectName: "scrollView"
        scrollViewProperties.scrollMode: ScrollMode.Vertical
        layoutProperties: AbsoluteLayoutProperties {
        }
        preferredWidth: 768.0
        translationY: 200.0
        Container {
        	objectName: "shopContainer"
            verticalAlignment: VerticalAlignment.Fill
            horizontalAlignment: HorizontalAlignment.Fill
            preferredWidth: 768.0
            layout: AbsoluteLayout {

            }
            WebImageView {
                objectName: "logo"
                preferredWidth: 400.0
                preferredHeight: 300.0
                translationX: 200.0
                scaletofit: true
            }
            Label {
                objectName: "headline"
                horizontalAlignment: HorizontalAlignment.Left
                textStyle.fontSize: FontSize.Large
                translationX: 25.0
                translationY: 350.0
                preferredWidth: 700.0
                preferredHeight: 100.0
                multiline: true
                textStyle.color: Color.create("#ffff7b00")
            }
            Divider {
                preferredWidth: 768.0
                translationY: 470.0
            }
            TextArea {
		        objectName: "aboutText"
                textStyle.color: Color.White
                backgroundVisible: false
                inputMode: TextAreaInputMode.Chat
                editable: false
                input.submitKey: SubmitKey.None
                translationX: 20.0
                translationY: 490.0
                preferredWidth: 730.0
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill
                textStyle.textAlign: TextAlign.Justify
            }
            Label {
                text: "KONTAKT"
                textStyle.fontSize: FontSize.Large
                textStyle.color: Color.create("#ffff7c00")
                translationX: 25.0
                translationY: 1060.0
            }
            Divider {
                preferredWidth: 768.0
                translationY: 1120.0
            }
            Label {
                text: "TELEFON:"
                translationX: 25.0
                translationY: 1200.0
            }
            TextArea {
                objectName: "telefon"
                textStyle.fontSize: FontSize.Small
                editable: false
                backgroundVisible: false
                translationY: 1184.0
                translationX: 200.0
            }
            Label {
                text: "HEMSIDA:"
                translationY: 1280.0
                translationX: 25.0
            }
            TextArea {
                objectName: "website"
                textStyle.fontSize: FontSize.Small
                editable: false
                backgroundVisible: false
                translationY: 1263.0
                translationX: 200.0
            }
            Label {
                text: "HITTA TILL BUTIKEN"
                textStyle.fontSize: FontSize.Large
                textStyle.color: Color.create("#ffff7c00")
                translationY: 1380.0
                translationX: 25.0
            }
            Divider {
                preferredWidth: 768.0
                translationY: 1440.0
            }
            WebImageView {
                objectName: "map"
                translationY: 1500.0
                translationX: 100.0
                scaletofit: true
                preferredWidth: 600.0
                preferredHeight: 600.0
            }
        }
    }
}