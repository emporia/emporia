// Default empty project template
import bb.cascades 1.00

Container {
    objectName: "page0Container"
    // PAGE BASE
    property string headlineString: "HEM"
    Container {
        layout: AbsoluteLayout {
        }
        ImageView {
	        id: imageView1
	        imageSource: "asset:///images/mobile-background.png"
	    }
	    ImageView {
	        id: imageView2
	        imageSource: "asset:///images/header-nav-bg.png"
	        horizontalAlignment: HorizontalAlignment.Fill
	        translationY: 34.0
	        scaleY: 2.60
	        maxWidth: 0.0
	        minWidth: 768.0
	    }
	    ImageView {
	        id: imageView3
	        imageSource: "asset:///images/emporia-logo.png"
	        scalingMethod: ScalingMethod.AspectFit
	        translationX: 60.0
	        translationY: 34.0
	        scaleX: 1.5
	        scaleY: 1.5
	        verticalAlignment: VerticalAlignment.Center
	        horizontalAlignment: HorizontalAlignment.Center
	    }
	    Label {
	        id: label1
	        text: headlineString
	        textStyle {
	            base: SystemDefaults.TextStyles.BodyText
	            color: Color.create(1, 0.5, 0.0)
	            fontStyle: FontStyle.Normal
	        }
	        translationX: 250.0
	        translationY: 25.0
	    }
	   Label {
	        id: label2
	        text: "MÅN - SÖN:"
	        textStyle {
	            base: SystemDefaults.TextStyles.SmallText
	            color: Color.create(1, 1, 0.8)
	            fontStyle: FontStyle.Normal
	        }
	        translationX: 250.0
	        translationY: 110.0
	    }
	   Label {
	        id: label3
	        text: "10 - 20"
	        textStyle {
	            base: SystemDefaults.TextStyles.SmallText
	            color: Color.create(1, 0.5, 0.0)
	            fontStyle: FontStyle.Normal
	        }
	        translationX: 380.0
	        translationY: 110.0
	    }
    }
}
