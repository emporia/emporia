// Default empty project template
import bb.cascades 1.00
import org.labsquare 1.0

Container {
    objectName: "page2Container"
    // PAGE BASE
    property string headlineString: "AKTUELLT"
    layout: AbsoluteLayout {
    }
    horizontalAlignment: HorizontalAlignment.Fill
    ImageView {
        id: imageView1
        imageSource: "asset:///images/mobile-background.png"
    }
    ImageView {
        id: imageView2
        imageSource: "asset:///images/header-nav-bg.png"
        horizontalAlignment: HorizontalAlignment.Fill
        translationY: 34.0
        scaleY: 2.60
        maxWidth: 0.0
        minWidth: 768.0
    }
    ImageView {
        id: imageView3
        imageSource: "asset:///images/emporia-logo.png"
        scalingMethod: ScalingMethod.AspectFit
        translationX: 60.0
        translationY: 34.0
        scaleX: 1.5
        scaleY: 1.5
    }
    Label {
        id: label1
        text: headlineString
        textStyle {
            base: SystemDefaults.TextStyles.BodyText
            color: Color.create(1, 0.5, 0.0)
            fontStyle: FontStyle.Normal
        }
        translationX: 250.0
        translationY: 25.0
    }
   Label {
        id: label2
        text: "MÅN - SÖN:"
        textStyle {
            base: SystemDefaults.TextStyles.SmallText
            color: Color.create(1, 1, 0.8)
            fontStyle: FontStyle.Normal
        }
        translationX: 250.0
        translationY: 110.0
    }
   Label {
        id: label3
        text: "10 - 20"
        textStyle {
            base: SystemDefaults.TextStyles.SmallText
            color: Color.create(1, 0.5, 0.0)
            fontStyle: FontStyle.Normal
        }
        translationX: 380.0
        translationY: 110.0
    }
    ListView {
    	objectName: "listView"
    	
        translationY: 195.0

        listItemComponents: ListItemComponent {
        	type: ""
        	Container {
                preferredWidth: 768.0
                preferredHeight: 300.0
                layout: AbsoluteLayout {

                }
                layoutProperties: AbsoluteLayoutProperties {

                }
                ImageView {
                    imageSource: "asset:///images/mobile-button-bg-def.png"
                    translationX: 0.0
                    translationY: 0.0
                    preferredWidth: 768.0
                    preferredHeight: 300.0
                    opacity: 0.5
                }
                Label {
                    text: ListItemData.name
                    textStyle.textAlign: TextAlign.Left
                    textStyle.fontSize: FontSize.Medium
                    textStyle.color: Color.create("#ffff7c00")
                    translationX: 25.0
                    translationY: 10.0
                }
                Label {
                    text: ListItemData.published
                    textStyle.color: Color.Gray
                    textStyle.fontSize: FontSize.Small
                    textStyle.fontStyle: FontStyle.Italic
                    textFormat: TextFormat.Plain
                    textStyle.textAlign: TextAlign.Left
                    translationX: 25.0
                    translationY: 60.0
                }
                WebImageView {
                    url: ListItemData.imageUrl
                    translationX: 550.0
                    translationY: 80.0
                    preferredWidth: 200.0
                    preferredHeight: 200.0
                    scaletofit: true
                }
                TextArea {
                    text: ListItemData.mainIntro
                    textStyle.color: Color.White
                    translationX: 10.0
                    translationY: 90.0
                    preferredWidth: 530.0
                    preferredHeight: 230.0
                    textStyle.fontSize: FontSize.XXSmall
                    horizontalAlignment: HorizontalAlignment.Fill
                    backgroundVisible: false
                    editable: false
                    input.submitKey: SubmitKey.None
                    textStyle.textAlign: TextAlign.Justify
                    inputMode: TextAreaInputMode.Chat
                }
                Label {
                	text: ListItemData.reloadText
                    textStyle.color: Color.create("#ffff7c00")
                    textStyle.textAlign: TextAlign.Center
                    translationX: 90.0
                    translationY: 100.0
                    textStyle.fontSize: FontSize.Large
                    textFormat: TextFormat.Plain
                }
            }
        }
        layoutProperties: AbsoluteLayoutProperties {

        }
        layout: StackListLayout {

        }
        preferredWidth: 768.0
        preferredHeight: 920.0
    }
}